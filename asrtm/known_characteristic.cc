/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "argo/asrtm/known_characteristic.h"
#include "argo/asrtm/log.h"

/** Optimize branch prediction for "untaken" */
#define unlikely(x)     __builtin_expect((x),0)


/** Logging macro (used only inside the metric) **/
#if LOG_ENABLED > 0
 #include <string>
 #include <iostream>
 #include <algorithm>
 #define PRINT_CHARACTERISTIC() do {\
   LOG_NEW_LINE("Constraint on '" + metricName +  "'") \
   std::string comparisonFun;\
   if (cFun == ComparisonFunction::Greater) {\
    comparisonFun = ">";\
   }\
   if (cFun == ComparisonFunction::GreaterOrEqual) {\
    comparisonFun = ">=";\
   }\
   if (cFun == ComparisonFunction::Less) {\
    comparisonFun = "<";\
   }\
   if (cFun == ComparisonFunction::LessOrEqual) {\
    comparisonFun = "<=";\
   }\
   LOG_NEW_LINE("OP valid if its value is " + comparisonFun + " " + std::to_string(goalValue))\
   int idZeroPaddingNumber =  std::to_string(sortedOPs.size() - 1).size();\
   int valueZeroPaddingNumber = 0;\
   for( auto it = opValues.begin(); it != opValues.end(); ++it) {\
    valueZeroPaddingNumber = std::max(valueZeroPaddingNumber, (int) std::to_string(*it).size());\
   }\
   int zeroPadding = std::max(idZeroPaddingNumber, valueZeroPaddingNumber);\
   INDENT()\
   LOG("OPs id: ")\
   for(auto it = sortedOPs.begin(); it != sortedOPs.end(); ++it) {\
    for(int i = 0; i < (zeroPadding - ((int) std::to_string(*it).size())); i++) {\
      LOG(" ")\
    }\
    LOG(std::to_string(*it) + " ")\
   }\
   NEW_LINE()\
   INDENT()\
   LOG("values: ")\
   for(auto it = sortedOPs.begin(); it != sortedOPs.end(); ++it) {\
    for(int i = 0; i < (zeroPadding - ((int) std::to_string(opValues[*it]).size())); i++) {\
      LOG(" ")\
    }\
    LOG(std::to_string(opValues[*it]) + " ")\
   }\
   NEW_LINE()\
   INDENT()\
   LOG(" limit: ")\
   for(auto it = sortedOPs.begin(); it != sortedOPs.end(); ++it) {\
    for(int i = 0; i < (zeroPadding - 1); i++) {\
      LOG(" ")\
    }\
    if(*it == *limitOP) {\
      LOG("* ")\
    } else {\
      LOG("  ")\
    }\
   }\
   NEW_LINE()\
   } while(0);
#else
 #define PRINT_CHARACTERISTIC()
#endif

namespace argo { namespace asrtm {
  
    

bool KnownCharacteristic::sortingFunction(KnownCharacteristic* obj, OperatingPointID_t lhs, OperatingPointID_t rhs) {
  return obj->comparisonFunction(obj->opValues[lhs], obj->opValues[rhs]);
}



KnownCharacteristic::KnownCharacteristic(std::string characteristicName, OperatingPointsList& opList, ComparisonFunction comparisonFunction, OPMetric_t goalValue, bool isRelaxable) {
  // default params
  this->isRelaxable = isRelaxable;
  difference = 0;

  // if relaxable exit
  if (unlikely(isRelaxable)) {
      return;
  }


  // check the size of the op list
  if (unlikely( opList.empty())) {
    throw std::logic_error("Error: The operating list is empty!");
  }

  // get the value of the ops  
  opValues.reserve(opList.size());
  metricName = characteristicName;
  for(std::vector<OperatingPoint>::iterator it = opList.begin(); it != opList.end(); ++it) {
    
    // check if the characteristic is a metric
    auto metricIterator = it->metrics.find(characteristicName);
    if (metricIterator != it->metrics.end()) {
      opValues.push_back(metricIterator->second);
      sortedOPs.push_back(it->id);
    } else { //is a parameter
      if (unlikely(it->parameters.find(characteristicName) == it->parameters.end())) {
	      throw std::logic_error("Error: Unable to find a metric or parameter named " + characteristicName);
      }
      
      opValues.push_back(it->parameters.find(characteristicName)->second);
      sortedOPs.push_back(it->id);
    }
  }
  
  
  // get the goal value and comparison function
  this->goalValue = goalValue;
  previousGoalValue = goalValue;
  cFun = comparisonFunction;
  
  // set the comparison functor
  if (cFun == ComparisonFunction::Greater)
    this->comparisonFunction = std::greater<OPMetric_t>();
  
  if (cFun == ComparisonFunction::GreaterOrEqual)
    this->comparisonFunction = std::greater_equal<OPMetric_t>();
  
  if (cFun == ComparisonFunction::Less)
    this->comparisonFunction = std::less<OPMetric_t>();
  
  if (cFun == ComparisonFunction::LessOrEqual)
    this->comparisonFunction = std::less_equal<OPMetric_t>();
  
  // sort the ops
  sortedOPs.sort(std::bind(&KnownCharacteristic::sortingFunction, this, std::placeholders::_1, std::placeholders::_2));
  
  
  // set the limitOP
  limitOP = sortedOPs.begin();
  if (!this->comparisonFunction(opValues[*limitOP], goalValue)) {
    return;
  }
  std::list<OperatingPointID_t>::iterator it = limitOP;
  for(++it; it != sortedOPs.end(); ++it) {    
    if (this->comparisonFunction(opValues[*it], goalValue)) {
      ++limitOP;
    } else {
      break;
    }
  }
}


  

bool KnownCharacteristic::updateModel(ModelPtr model, OperatingPointID_t currentOP_ID) {

  LOG_NEW_LINE("initial situation")
  PRINT_CHARACTERISTIC()
   
  // init the result
  bool result = false;

  // check if the limitOP is valid (in the case that no op satisfy the goal)
  bool limitOPValid = true;
  if (!comparisonFunction(opValues[*limitOP], previousGoalValue)) {
    limitOPValid = false;
  }

  if ((cFun == ComparisonFunction::Greater) || (cFun == ComparisonFunction::GreaterOrEqual)) {
    difference = - difference;
  }

  LOG_NEW_LINE("goal difference: " + std::to_string(difference))
  
  // checking if we need to add or remove OP from S1
  if (difference < 0) {
    
    // *****************  theoretically we need to remove OPs from S1 (the situation get worse)
    LOG_NEW_LINE("checking if we need to remove op from S1")
    ADD_INDENT()
    
    // checking if the situation actually change
    while(!comparisonFunction(opValues[*limitOP], goalValue)) {
      LOG_NEW_LINE("removed op " + std::to_string(*limitOP))
      
      if (*limitOP == currentOP_ID) {
	      result = true;
      }
      
      model->removeOperatingPointFromS1(*limitOP);
      
      // check boundaries
      if (limitOP == sortedOPs.begin()) {
	      break;
      }
      
      --limitOP;
    }
    REMOVE_INDENT()
    LOG_NEW_LINE("check done")
    
  } else  {
    // *****************  theoretically we need to add OP to S2 (the situation get better)
    
    // check if the limitOP is valid (in the case that no op satisfy the goal)
    if (limitOPValid) {
      ++limitOP;
    }
    
    LOG_NEW_LINE("checking if we need to add op to S2")
    ADD_INDENT()
    
    // checking if the situation actually change
    bool isAdded = true;
    while(isAdded) {
      
      // check boundaries
      if (limitOP == sortedOPs.end()) {
	      break;
      }

      INDENT()
      LOG("checking op " + std::to_string(*limitOP))
      
      // check if it satisfy the goal
      isAdded = comparisonFunction(opValues[*limitOP], goalValue);
      if (isAdded) {
	      model->addOperatingPoint(*limitOP);
	      ++limitOP;
        LOG(", added!")
      } else {
	      isAdded = false;
        LOG(", not valid, skip the other ops")
      }
      NEW_LINE()
      
    }
    REMOVE_INDENT()
    LOG_NEW_LINE("check done")
    
    // because limitOP is the closest op that satisfy the constraint
    if (limitOP != sortedOPs.begin()) {
      --limitOP;
    }
  }
  
  difference = 0;
  LOG_NEW_LINE("new situation:")
  PRINT_CHARACTERISTIC()
  return result;
}


KnownCharacteristic::KnownCharacteristic(const KnownCharacteristic &characteristic) {

    // copy the simpler attributes
    this->difference = characteristic.difference;
    this->isRelaxable = characteristic.isRelaxable;
    this->callbackFunction = characteristic.callbackFunction;
    this->goalValue = characteristic.goalValue;
    this->cFun = characteristic.cFun;
    this->metricName = characteristic.metricName;
    this->limitOP = characteristic.limitOP;

    // copy the lists
    for(std::list<argo::asrtm::OperatingPointID_t>::const_iterator op = characteristic.sortedOPs.cbegin(); op != characteristic.sortedOPs.cend(); ++op) {
        this->sortedOPs.push_back(*op);
    }

    this->opValues.reserve(characteristic.opValues.size());
    for(std::vector<argo::asrtm::OPParameter_t>::const_iterator op = characteristic.opValues.cbegin(); op != characteristic.opValues.cend(); ++op) {
        this->opValues.push_back(*op);
    }
}

KnownCharacteristic::~KnownCharacteristic() {
    sortedOPs.clear();
    opValues.clear();
}



  

bool KnownCharacteristic::isAdmissible(OperatingPointID_t opID) {
  return comparisonFunction(opValues[opID], goalValue);
}

OPMetric_t KnownCharacteristic::getOPValue(OperatingPointID_t opID) {
  return opValues[opID];
}
  
  

void KnownCharacteristic::checkRelaxable(OperatingPointID_t currentOP) {
  if (!comparisonFunction(opValues[currentOP], goalValue)) {
    callbackFunction();
  }
}



void KnownCharacteristic::clearWindow() {
}


std::list<OperatingPointID_t>::iterator KnownCharacteristic::getWorstOP() {
  std::list<OperatingPointID_t>::iterator worstOP = sortedOPs.end();
  --worstOP;
  return worstOP;  
}


std::list<OperatingPointID_t>::iterator KnownCharacteristic::getBestOP() {
  return sortedOPs.begin();  
}


std::list<OperatingPointID_t>::iterator KnownCharacteristic::getLimitOP() {
  return limitOP;  
}


OPMetric_t KnownCharacteristic::getDistance(OperatingPointID_t op) {
  double diff = goalValue - opValues[op];  
  return std::abs(diff);
}



void KnownCharacteristic::changeGoalValue(OPMetric_t newValue) {
  difference += newValue - goalValue;  
  previousGoalValue = goalValue;
  goalValue = newValue;
}

void KnownCharacteristic::incrementGoal(OPMetric_t increment) {
  OPMetric_t delta = goalValue*increment;
  difference += delta;
  previousGoalValue = goalValue;
  goalValue += delta;
}


void KnownCharacteristic::setRelaxable(std::function< void() > callbackFunction) {
  this->callbackFunction = callbackFunction;
}


} // namespace asrtm

} // namespace argo
