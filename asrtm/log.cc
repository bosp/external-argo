/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 #include "argo/asrtm/log.h"
 #include <iostream>

 using namespace std;
 using namespace argo;


 void Log::addIndent() {
 	Log::indentLevel++;
 }

 void Log::removeIndent() {
 	Log::indentLevel--;
 }

 void Log::logNewLine(std::string what) {
 	for (int i = 0; i < Log::indentLevel*Log::whiteSpaceTick; i++) {
 		cout << " ";
 	}
 	cout << what << endl;
 }

 void Log::log(std::string what) {
 	cout << what;
 }

 void Log::indent() {
 	for (int i = 0; i < Log::indentLevel*Log::whiteSpaceTick; i++) {
 		cout << " ";
 	}
 }

 void Log::newLine() {
 	cout << endl;
 }


 uint8_t Log::indentLevel = 0;
