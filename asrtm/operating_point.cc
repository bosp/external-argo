#include "argo/asrtm/operating_point.h"

#include <utility>


namespace argo {
namespace asrtm {


OperatingPoint::OperatingPoint(std::map<std::string, OPParameter_t> parameters,
                               std::map<std::string, OPMetric_t>  metrics) :
    parameters(parameters),
    metrics(metrics),
    states({"default"}) {
id = 0;
}



OperatingPoint::OperatingPoint(std::map<std::string, OPParameter_t> parameters,
                               std::map<std::string, OPMetric_t>  metrics,
                               std::list<std::string> states) :
                               parameters(parameters),
                               metrics(metrics),
                               states(states) {
    id = 0;
}



OperatingPoint::~OperatingPoint() {
    parameters.clear();
    metrics.clear();
    states.clear();
}



OperatingPoint::OperatingPoint(const OperatingPoint &op) {

    // copy the params
    for(ApplicationParameters::const_iterator param = op.parameters.cbegin(); param != op.parameters.cend(); ++param) {
        this->parameters.insert(std::pair<std::string, OPParameter_t>(param->first, param->second));
    }

    // copy the metrics
    for(ApplicationMetrics::const_iterator metric = op.metrics.cbegin(); metric != op.metrics.cend(); ++metric) {
        this->metrics.insert(std::pair<std::string, OPMetric_t>(metric->first, metric->second));
    }

    // copy the states
    for(std::list<std::string>::const_iterator state = op.states.cbegin(); state != op.states.cend(); ++state) {
        this->states.push_back(*state);
    }

    // copy the others attribute
    this->id = op.id;
}





}
}
