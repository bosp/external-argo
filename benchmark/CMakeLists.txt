include_directories(${source_dir}/include)


# Set the tests list
set( BENCHMARK_HDR 
 )

set( BENCHMARK_SRC ${ARGO_SOURCE_DIR}/benchmark/main.cc
)

# Add test executable target
add_executable(Benchmark ${BENCHMARK_SRC} ${TESTS_HDR} )



if( LIB_STATIC )
	target_link_libraries(Benchmark argo_monitorStatic)
else( LIB_STATIC )
	target_link_libraries(Benchmark argo_monitor)
endif( LIB_STATIC )

if( LIB_STATIC )
	target_link_libraries(Benchmark argo_asrtmStatic)
else( LIB_STATIC )
	target_link_libraries(Benchmark argo_asrtm)
endif( LIB_STATIC )


install( TARGETS Benchmark DESTINATION bin )

