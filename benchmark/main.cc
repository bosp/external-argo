#include <iostream>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <memory>
#include "argo/asrtm/asrtm.h"
#include "argo/monitor/monitor.h"

using namespace std;

enum class Directions { Up, Down, Random };


const std::vector<argo::asrtm::OperatingPointID_t> opSizes = { 10, 20, 50, 100, 150, 200, 300, 400, 500, 1000 };
const std::vector<Directions> directions = { Directions::Up, Directions::Down, Directions::Up, Directions::Random };
const unsigned num_trial = 10;



void init_op_list(argo::asrtm::OperatingPointID_t num_op, argo::asrtm::OperatingPointsList& op_list) {

    op_list.clear();

    // for every operating point
    for(argo::asrtm::OperatingPointID_t op_id = 0; op_id < num_op; op_id++ ) {

        // create the operating point
        argo::asrtm::OperatingPoint op;

        // add the param
        op.parameters.insert(std::pair<std::string, argo::asrtm::OPParameter_t>("param", op_id));

        // populate the metrics
        unsigned metric_counter = 0;
        for( std::vector<Directions>::const_iterator d = directions.cbegin(); d != directions.cend(); ++d, metric_counter++) {

            argo::asrtm::OPMetric_t metric_value = 0;

            if (*d == Directions::Up)
                metric_value = op_id;


            if (*d == Directions::Down)
                metric_value = num_op - op_id - 1;


            if (*d == Directions::Random)
                metric_value = rand() % 10000;

            op.metrics.insert(std::pair<std::string, argo::asrtm::OPMetric_t>("metric" + std::to_string(metric_counter), metric_value));

        }

        op.states.push_back("default");

        op_list.push_back(op);

    }
}



void init_benchmark(std::vector<argo::asrtm::Asrtm>& benchmark) {
    benchmark.clear();

    for(std::vector<argo::asrtm::OperatingPointID_t>::const_iterator num_op = opSizes.cbegin(); num_op != opSizes.cend(); ++num_op) {

        argo::asrtm::OperatingPointsList op_list;

        init_op_list(*num_op, op_list);

        argo::asrtm::Asrtm asrtm = argo::asrtm::Asrtm(op_list);

        benchmark.push_back(asrtm);
    }

}








void testRank() {

    // open report file
    fstream report;
    report.open("rank_test.csv", ios::out | ios::trunc);
    report << "Num OP, Time[us]" << endl;


    cout << "Rank Test Report: " << endl;


    // boot_strap
    std::vector<argo::asrtm::Asrtm> benchmark;
    init_benchmark(benchmark);


    // create the monitor
    argo::monitor::TimeMonitorPtr monitor(new argo::monitor::TimeMonitor(argo::monitor::TimeMeasure::Microseconds, 1));

    // the result vector
    std::vector<argo::monitor::Time_t> results;
    results.resize(benchmark.size());


    // loop over the interested durations
    for(unsigned index = 0; index < num_trial; index++) {

        // init step


        // elaboration step
        unsigned bench_it = 0;
        for(std::vector<argo::asrtm::Asrtm>::iterator it = benchmark.begin(); it != benchmark.end(); ++it, bench_it++) {
            monitor->start();
            it->setGeometricRank({{"metric0", 1}, {"metric1", 2}, {"metric2", 3}}, argo::RankObjective::Maximize);
            monitor->stop();
            results[bench_it] += monitor->getLastElement();
        }

        // undo step

    }


    // compute the average
    for(unsigned index = 0; index < results.size(); index++ ) {
        results[index] /= results.size();
    }


    // print the results, both on file and on video
    for(unsigned index = 0; index < results.size(); index++) {
        cout << "#OP: " << opSizes[index] << " -> " << results[index] << "us" << endl;
        report << opSizes[index] << ", " << results[index] << endl;
    }

    report.close();
    cout << endl << endl;
}




void testAddDynamicConstraint(argo::monitor::WindowSize size) {

    // open report file
    fstream report;
    report.open("dynamic_constraint_" + std::to_string(size) + "_element.csv", ios::out | ios::trunc);
    report << "Num OP, Time[us]" << endl;


    cout << "Dynamic constraint " << size << " element Report: " << endl;


    // boot_strap
    std::vector<argo::asrtm::Asrtm> benchmark;
    init_benchmark(benchmark);

    // create a fake monitor
    shared_ptr<argo::monitor::DataBuffer<double>> test( new argo::monitor::DataBuffer<double>(size));
    shared_ptr<argo::monitor::Goal<double>> goal( new argo::monitor::Goal<double>(test, argo::DataFunction::Average, argo::ComparisonFunction::Greater, static_cast<double>(1000)));



    // create the monitor
    argo::monitor::TimeMonitorPtr monitor(new argo::monitor::TimeMonitor(argo::monitor::TimeMeasure::Microseconds, 1));

    // the result vector
    std::vector<argo::monitor::Time_t> results;
    results.resize(benchmark.size());


    // loop over the interested durations
    for(unsigned index = 0; index < num_trial; index++) {

        // init step


        // elaboration step
        unsigned bench_it = 0;
        for(std::vector<argo::asrtm::Asrtm>::iterator it = benchmark.begin(); it != benchmark.end(); ++it, bench_it++) {
            monitor->start();
            it->addDynamicConstraintOnTop(goal, "metric0", 0.3, 1);
            monitor->stop();
            results[bench_it] += monitor->getLastElement();
        }

        // undo step
        for(std::vector<argo::asrtm::Asrtm>::iterator it = benchmark.begin(); it != benchmark.end(); ++it, bench_it++) {
            it->removeConstraintOnTop();
        }



    }


    // compute the average
    for(unsigned index = 0; index < results.size(); index++ ) {
        results[index] /= results.size();
    }


    // print the results, both on file and on video
    for(unsigned index = 0; index < results.size(); index++) {
        cout << "#OP: " << opSizes[index] << " -> " << results[index] << "us" << endl;
        report << opSizes[index] << ", " << results[index] << endl;
    }

    report.close();
    cout << endl << endl;
}




void testFixedDifference(argo::monitor::WindowSize size) {

    // open report file
    fstream report;
    report.open("fixed_difference_" + std::to_string(size) + "_element.csv", ios::out | ios::trunc);
    report << "Num OP, Time[us]" << endl;


    cout << "Fixed Difference " << size << " element Report: " << endl;


    // boot_strap
    std::vector<argo::asrtm::Asrtm> benchmark;
    init_benchmark(benchmark);

    // set rank
    for(std::vector<argo::asrtm::Asrtm>::iterator it = benchmark.begin(); it != benchmark.end(); ++it) {
        it->setSimpleRank("metric0", argo::RankObjective::Maximize);
    }



    // create a fake monitor
    shared_ptr<argo::monitor::DataBuffer<double>> test( new argo::monitor::DataBuffer<double>(size));
    shared_ptr<argo::monitor::Goal<double>> goal( new argo::monitor::Goal<double>(test, argo::DataFunction::Average, argo::ComparisonFunction::GreaterOrEqual, static_cast<double>(1)));


    // add the dynamic constraint
    for(unsigned index = 0; index < benchmark.size(); index++) {
        benchmark[index].addDynamicConstraintOnTop(goal, "metric0", 0.3, 1);
    }



    // create the monitor
    argo::monitor::TimeMonitorPtr monitor(new argo::monitor::TimeMonitor(argo::monitor::TimeMeasure::Microseconds, 1));

    // the result vector
    std::vector<argo::monitor::Time_t> results;
    results.resize(benchmark.size());


    // loop over the interested durations
    for(unsigned index = 0; index < num_trial; index++) {

        // init step
        goal->setGoalValue(9);
        for(unsigned index = 0; index < size; index++) {
            test->push(1);
        }


        // elaboration step
        for(unsigned index = 0; index < benchmark.size(); index++) {
            monitor->start();
            benchmark[index].getBestApplicationParameters();
            monitor->stop();
            results[index] += monitor->getLastElement();
        }

        // undo step
        goal->setGoalValue(1);
        test->clear();
        for(unsigned index = 0; index < benchmark.size(); index++) {
            benchmark[index].getBestApplicationParameters();
        }



    }


    // compute the average
    for(unsigned index = 0; index < results.size(); index++ ) {
        results[index] /= results.size();
    }


    // print the results, both on file and on video
    for(unsigned index = 0; index < results.size(); index++) {
        cout << "#OP: " << opSizes[index] << " -> " << results[index] << "us" << endl;
        report << opSizes[index] << ", " << results[index] << endl;
    }

    report.close();
    cout << endl << endl;
}




void testSearchS1(argo::monitor::WindowSize size) {

    // open report file
    fstream report;
    report.open("search_S1_" + std::to_string(size) + "_element.csv", ios::out | ios::trunc);
    report << "Num OP, Time[us]" << endl;


    cout << "Search S1 " << size << " element Report: " << endl;


    // boot_strap
    std::vector<argo::asrtm::Asrtm> benchmark;
    init_benchmark(benchmark);

    // set rank
    for(std::vector<argo::asrtm::Asrtm>::iterator it = benchmark.begin(); it != benchmark.end(); ++it) {
        it->setSimpleRank("metric0", argo::RankObjective::Minimize);
    }



    // create a fake monitor
    shared_ptr<argo::monitor::DataBuffer<double>> test( new argo::monitor::DataBuffer<double>(size));
    shared_ptr<argo::monitor::Goal<double>> goal( new argo::monitor::Goal<double>(test, argo::DataFunction::Average, argo::ComparisonFunction::GreaterOrEqual, static_cast<double>(1)));


    // add the dynamic constraint
    for(unsigned index = 0; index < benchmark.size(); index++) {
        benchmark[index].addDynamicConstraintOnTop(goal, "metric0", 0.3, 1);
    }



    // create the monitor
    argo::monitor::TimeMonitorPtr monitor(new argo::monitor::TimeMonitor(argo::monitor::TimeMeasure::Microseconds, 1));

    // the result vector
    std::vector<argo::monitor::Time_t> results;
    results.resize(benchmark.size());


    // loop over the interested durations
    for(unsigned index = 0; index < num_trial; index++) {

        // init step
        goal->setGoalValue(2);
        for(unsigned index = 0; index < size; index++) {
            test->push(1);
        }


        // elaboration step
        for(unsigned index = 0; index < benchmark.size(); index++) {
            monitor->start();
            benchmark[index].getBestApplicationParameters();
            monitor->stop();
            results[index] += monitor->getLastElement();
        }

        // undo step
        goal->setGoalValue(1);
        test->clear();
        for(unsigned index = 0; index < benchmark.size(); index++) {
            benchmark[index].getBestApplicationParameters();
        }



    }


    // compute the average
    for(unsigned index = 0; index < results.size(); index++ ) {
        results[index] /= results.size();
    }


    // print the results, both on file and on video
    for(unsigned index = 0; index < results.size(); index++) {
        cout << "#OP: " << opSizes[index] << " -> " << results[index] << "us" << endl;
        report << opSizes[index] << ", " << results[index] << endl;
    }

    report.close();
    cout << endl << endl;
}




void testUnsatisfiable(argo::monitor::WindowSize size) {

    // open report file
    fstream report;
    report.open("unsatisfiable_" + std::to_string(size) + "_element.csv", ios::out | ios::trunc);
    report << "Num OP, Time[us]" << endl;


    cout << "Unsatisfiable " << size << " element Report: " << endl;


    // boot_strap
    std::vector<argo::asrtm::Asrtm> benchmark;
    init_benchmark(benchmark);

    // set rank
    for(std::vector<argo::asrtm::Asrtm>::iterator it = benchmark.begin(); it != benchmark.end(); ++it) {
        it->setSimpleRank("metric0", argo::RankObjective::Minimize);
    }



    // create a fake monitor
    shared_ptr<argo::monitor::DataBuffer<double>> test( new argo::monitor::DataBuffer<double>(size));
    shared_ptr<argo::monitor::Goal<double>> goal( new argo::monitor::Goal<double>(test, argo::DataFunction::Average, argo::ComparisonFunction::GreaterOrEqual, static_cast<double>(1)));


    // add the constraints
    for(unsigned index = 0; index < benchmark.size(); index++) {
        benchmark[index].addDynamicConstraintOnTop(goal, "metric0", 0.3, 1);
        benchmark[index].addStaticConstraintOnBottom("metric1", argo::ComparisonFunction::Greater, 0);
    }



    // create the monitor
    argo::monitor::TimeMonitorPtr monitor(new argo::monitor::TimeMonitor(argo::monitor::TimeMeasure::Microseconds, 1));

    // the result vector
    std::vector<argo::monitor::Time_t> results;
    results.resize(benchmark.size());


    // loop over the interested durations
    for(unsigned index = 0; index < num_trial; index++) {

        // init step
        for(unsigned index = 0; index < size; index++) {
            test->push(1);
        }


        // elaboration step
        for(unsigned index = 0; index < benchmark.size(); index++) {
            benchmark[index].changeStaticConstraintGoalValue(1, opSizes[index] + 1);
            monitor->start();
            benchmark[index].getBestApplicationParameters();
            monitor->stop();
            results[index] += monitor->getLastElement();
        }

        // undo step
        goal->setGoalValue(1);
        test->clear();
        for(unsigned index = 0; index < benchmark.size(); index++) {
            benchmark[index].changeStaticConstraintGoalValue(1, 0);
            benchmark[index].getBestApplicationParameters();
        }



    }


    // compute the average
    for(unsigned index = 0; index < results.size(); index++ ) {
        results[index] /= results.size();
    }


    // print the results, both on file and on video
    for(unsigned index = 0; index < results.size(); index++) {
        cout << "#OP: " << opSizes[index] << " -> " << results[index] << "us" << endl;
        report << opSizes[index] << ", " << results[index] << endl;
    }

    report.close();
    cout << endl << endl;
}



void testSatisfiable(argo::monitor::WindowSize size) {

    // open report file
    fstream report;
    report.open("satisfiable_" + std::to_string(size) + "_element.csv", ios::out | ios::trunc);
    report << "Num OP, Time[us]" << endl;


    cout << "Satisfiable " << size << " element Report: " << endl;


    // boot_strap
    std::vector<argo::asrtm::Asrtm> benchmark;
    init_benchmark(benchmark);

    // set rank
    for(std::vector<argo::asrtm::Asrtm>::iterator it = benchmark.begin(); it != benchmark.end(); ++it) {
        it->setSimpleRank("metric0", argo::RankObjective::Minimize);
    }



    // create a fake monitor
    shared_ptr<argo::monitor::DataBuffer<double>> test( new argo::monitor::DataBuffer<double>(size));
    shared_ptr<argo::monitor::Goal<double>> goal( new argo::monitor::Goal<double>(test, argo::DataFunction::Average, argo::ComparisonFunction::GreaterOrEqual, static_cast<double>(1)));


    // add the constraints
    for(unsigned index = 0; index < benchmark.size(); index++) {
        benchmark[index].addDynamicConstraintOnTop(goal, "metric0", 0.3, 1);
        benchmark[index].addStaticConstraintOnBottom("metric1", argo::ComparisonFunction::Greater, opSizes[index] + 1);
    }



    // create the monitor
    argo::monitor::TimeMonitorPtr monitor(new argo::monitor::TimeMonitor(argo::monitor::TimeMeasure::Microseconds, 1));

    // the result vector
    std::vector<argo::monitor::Time_t> results;
    results.resize(benchmark.size());


    // loop over the interested durations
    for(unsigned index = 0; index < num_trial; index++) {

        // init step
        for(unsigned index = 0; index < size; index++) {
            test->push(1);
        }


        // elaboration step
        for(unsigned index = 0; index < benchmark.size(); index++) {
            benchmark[index].changeStaticConstraintGoalValue(1, 0);
            monitor->start();
            benchmark[index].getBestApplicationParameters();
            monitor->stop();
            results[index] += monitor->getLastElement();
        }

        // undo step
        goal->setGoalValue(1);
        test->clear();
        for(unsigned index = 0; index < benchmark.size(); index++) {
            benchmark[index].changeStaticConstraintGoalValue(1, opSizes[index] + 1);
            benchmark[index].getBestApplicationParameters();
        }



    }


    // compute the average
    for(unsigned index = 0; index < results.size(); index++ ) {
        results[index] /= results.size();
    }


    // print the results, both on file and on video
    for(unsigned index = 0; index < results.size(); index++) {
        cout << "#OP: " << opSizes[index] << " -> " << results[index] << "us" << endl;
        report << opSizes[index] << ", " << results[index] << endl;
    }

    report.close();
    cout << endl << endl;
}




void testPush() {

    // open report file
    fstream report;
    report.open("push_test.csv", ios::out | ios::trunc);
    report << "Num Element, Time[us]" << endl;


    const std::vector<argo::monitor::WindowSize> elements = {10, 20, 50, 100, 200, 300, 500, 1000 };

    cout << "Push element Report: " << endl;

    shared_ptr<argo::monitor::DataBuffer<float>> test( new argo::monitor::DataBuffer<float>());


    // create the monitor
    argo::monitor::TimeMonitorPtr monitor(new argo::monitor::TimeMonitor(argo::monitor::TimeMeasure::Microseconds, 1));

    // the result vector
    std::vector<argo::monitor::Time_t> results;


    results.resize(elements.size(), 0);


    // loop over the interested durations
    for(unsigned index = 0; index < num_trial; index++) {



        for(unsigned e = 0; e < elements.size(); e++) {

            test->resize(elements[e]);


            // init step
            monitor->start();
            for(unsigned i = 0; i < elements[e]; i++) {
                test->push(1);
            }
            monitor->stop();
            results[e] += monitor->getLastElement();




            test->clear();
        }




    }


    // compute the average
    for(unsigned index = 0; index < results.size(); index++ ) {
        results[index] /= results.size();
    }


    // print the results, both on file and on video
    for(unsigned index = 0; index < results.size(); index++) {
        cout << "#Elements: " << elements[index] << " -> " << results[index] << "us" << endl;
        report << elements[index] << ", " << results[index] << endl;
    }

    report.close();
    cout << endl << endl;
}






int main() {

    cout << "Let the benchmark begin! " << endl;
    testRank();

    testPush();


    testAddDynamicConstraint(1);


    testFixedDifference(1);


    testSearchS1(1);


    testUnsatisfiable(1);



    testSatisfiable(1);




    cout << "Benchmark done" << endl;

    return 1;
}
