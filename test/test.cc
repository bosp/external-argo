#include <iostream>



#include "gtest/gtest.h"


#include "testMonitors.h"
#include "testKC.h"
#include "testOC.h"
#include "testOPManager.h"
#include "testAsrtm.h"
#include "testDataBuffer.h"
#include "testGoal.h"








int main(int argc, char* argv[]) {
  // set to print the elapsed time
  ::testing::GTEST_FLAG(print_time) = false;

  // start the tests
  ::testing::InitGoogleTest(&argc, argv);

  // done
  int result = RUN_ALL_TESTS();
  std::cout << std::flush;
  sleep(1);
  return result;
}
