#include <memory>
#include <cstdint>
#include <vector>
#include <random>
#include <algorithm>
#include <iostream>
#include <list>
#include "gtest/gtest.h"

#include "argo/monitor/memory_monitor.h"
#include "argo/monitor/time_monitor.h"
#include "argo/monitor/throughput_monitor.h"
#include "argo/monitor/process_cpu_usage_monitor.h"
#include "argo/monitor/system_cpu_usage_monitor.h"




TEST(Monitors, memoryMonitor) {

    argo::monitor::MemoryMonitorPtr m(new argo::monitor::MemoryMonitor());

    m->extractMemoryUsage();

    ASSERT_LT(static_cast<argo::monitor::Memory_t>(0), m->get(argo::DataFunction::Average));

    ASSERT_LT(static_cast<argo::monitor::Memory_t>(0), m->extractVmPeakSize());


}


TEST(Monitors, timeMonitor) {

    argo::monitor::TimeMonitorPtr m(new argo::monitor::TimeMonitor(argo::monitor::TimeMeasure::Microseconds));

    m->start();
    usleep(150);
    m->stop();
    ASSERT_LE(static_cast<argo::monitor::Time_t>(150), m->get(argo::DataFunction::Average));


    m = argo::monitor::TimeMonitorPtr(new argo::monitor::TimeMonitor());

    m->start();
    usleep(50000);
    m->stop();
    ASSERT_EQ(static_cast<argo::monitor::Time_t>(50), m->get(argo::DataFunction::Average));


    m = argo::monitor::TimeMonitorPtr(new argo::monitor::TimeMonitor(argo::monitor::TimeMeasure::Seconds));

    m->start();
    sleep(3);
    m->stop();
    ASSERT_EQ(static_cast<argo::monitor::Time_t>(3), m->get(argo::DataFunction::Average));

}


TEST(Monitors, throughputMonitor) {

    argo::monitor::ThroughputMonitorPtr m(new argo::monitor::ThroughputMonitor());

    m->start();
    sleep(1);
    m->stop(1);
    ASSERT_GE(1, m->get(argo::DataFunction::Average));
    ASSERT_LT(0.99, m->get(argo::DataFunction::Average));

}



TEST(Monitors, processUsageMonitor) {

    argo::monitor::ProcessCpuUsageMonitorPtr m(new argo::monitor::ProcessCpuUsageMonitor());

    m->start();

    int dim_vett = 100;
    long unsigned int iteration_number = 10000;

    std::vector<double> vett;
    vett.resize(dim_vett);
    for(int i = 0; i < dim_vett; i++) {
        vett[i] = i;
    }

    std::random_device rd;
    std::mt19937 g(rd());


    for(long unsigned int i = 0; i < iteration_number; i++) {
        std::shuffle(vett.begin(), vett.end(), g);
    }

    m->stop();

    ASSERT_GT(1.1, m->get(argo::DataFunction::Average));
    ASSERT_LT(0.9, m->get(argo::DataFunction::Average));

}





TEST(Monitors, systemUsageMonitor) {

    argo::monitor::SystemCpuUsageMonitorPtr m(new argo::monitor::SystemCpuUsageMonitor());


    m->start();

    int dim_vett = 100;
    long unsigned int iteration_number = 10000;

    std::vector<double> vett;
    vett.resize(dim_vett);
    for(int i = 0; i < dim_vett; i++) {
        vett[i] = i;
    }

    std::random_device rd;
    std::mt19937 g(rd());


    for(long unsigned int i = 0; i < iteration_number; i++) {
        std::shuffle(vett.begin(), vett.end(), g);
    }

    m->stop();

    std::cout << "    ---> I can't really test it, it should be around 1.0 in isolation, i got " << m->get(argo::DataFunction::Average) << std::endl;

}
