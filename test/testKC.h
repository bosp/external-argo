#include <memory>
#include <cstdint>
#include <vector>
#include <list>
#include "gtest/gtest.h"

#include "argo/asrtm/known_characteristic.h"
#include "argo/asrtm/model.h"


int test_kc = 0;

void my_callback_kc() {
  test_kc = 4;
}


TEST(KnownCharacteristic, MetricNotFoundWithGoal) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "something", 10 },
          },
        }
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }



  ASSERT_THROW({std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("something_else", OPList, argo::ComparisonFunction::LessOrEqual, 20));}, std::logic_error);
}


TEST(KnownCharacteristic, ZeroOPWithGoal) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
  };

  // create the observable characteristic
  ASSERT_THROW({std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("foo", OPList, argo::ComparisonFunction::LessOrEqual, 20));}, std::logic_error);
}

TEST(KnownCharacteristic, SingleOPWithGoal) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "foo", 10 },
          },
        }
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

  // create the observable characteristic
  std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("foo", OPList, argo::ComparisonFunction::Greater, 10));

  // check the best op
  EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getBestOP()));

  // check the limit op
  EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getLimitOP()));

  // check the worst op
  EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getWorstOP()));
}

TEST(KnownCharacteristic, RelaxableConstraintCall) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "foo", 10 },
          },
        }
  };

  // create the observable characteristic
  std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("foo", OPList, argo::ComparisonFunction::Greater, 10));
  kc->setRelaxable(my_callback_kc);

  test_kc = 0;
  kc->checkRelaxable(0);

  // check calling
  EXPECT_EQ(4, test_kc);
}

TEST(KnownCharacteristic, RelaxableConstraintNoCall) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "foo", 10 },
          },
        }
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

  // create the observable characteristic
  std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("foo", OPList, argo::ComparisonFunction::Greater, 9));
  kc->setRelaxable(my_callback_kc);

  test_kc = 0;
  kc->checkRelaxable(0);

  // check calling
  EXPECT_EQ(0, test_kc);
}


TEST(KnownCharacteristic, DescendingOrdering) {
  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 10 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 4
        { // parameters
          { "param", 4 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
      { // OP 5
        { // parameters
          { "param", 5 },
        },
        { // metrics
          { "fps", 50 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

  // create the observable characteristic
  std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Greater, 25));

  // check the best op
  EXPECT_EQ((argo::asrtm::OPMetric_t) 4, *(kc->getBestOP()));

  // check the limit op
  EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getLimitOP()));

  // check the worst op
  EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getWorstOP()));
}

TEST(KnownCharacteristic, AscendingOrdering) {
  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 10 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 4
        { // parameters
          { "param", 4 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
      { // OP 5
        { // parameters
          { "param", 5 },
        },
        { // metrics
          { "fps", 50 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

  // create the observable characteristic
  std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Less, 30));

  // check the best op
  EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getBestOP()));

  // check the limit op
  EXPECT_EQ((argo::asrtm::OPMetric_t) 1, *(kc->getLimitOP()));

  // check the worst op
  EXPECT_EQ((argo::asrtm::OPMetric_t) 4, *(kc->getWorstOP()));
}

TEST(KnownCharacteristic, AdmissibleGreater) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Greater, 30));

    // check the validity
    EXPECT_FALSE(kc->isAdmissible(0));
    EXPECT_FALSE(kc->isAdmissible(1));
    EXPECT_TRUE(kc->isAdmissible(2));
}

TEST(KnownCharacteristic, AdmissibleGreaterOrEqual) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::GreaterOrEqual, 30));

    // check the validity
    EXPECT_FALSE(kc->isAdmissible(0));
    EXPECT_TRUE(kc->isAdmissible(1));
    EXPECT_TRUE(kc->isAdmissible(2));
}

TEST(KnownCharacteristic, AdmissibleLess) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "power", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "power", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "power", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("power", OPList, argo::ComparisonFunction::Less, 30));

    // check the validity
    EXPECT_TRUE(kc->isAdmissible(0));
    EXPECT_FALSE(kc->isAdmissible(1));
    EXPECT_FALSE(kc->isAdmissible(2));
}

TEST(KnownCharacteristic, AdmissibleLessOrEqual) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "power", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "power", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "power", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("power", OPList, argo::ComparisonFunction::LessOrEqual, 30));

    // check the validity
    EXPECT_TRUE(kc->isAdmissible(0));
    EXPECT_TRUE(kc->isAdmissible(1));
    EXPECT_FALSE(kc->isAdmissible(2));
}


TEST(KnownCharacteristic, ShiftOPLimitUPWithGoalDescending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Greater, 100));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OPMetric_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (kc->isAdmissible(id)) {
        model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());

    // change the goal
    kc->changeGoalValue(25);

    // update the model
    EXPECT_FALSE(kc->updateModel(model, *(kc->getLimitOP())));

    // check the model size
    EXPECT_EQ((unsigned) 2, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());

    // check the situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 1, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getWorstOP()));

    // change the goal
    kc->changeGoalValue(0);

    // update the model
    EXPECT_FALSE(kc->updateModel(model, *(kc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 3, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());
}

TEST(KnownCharacteristic, ShiftOPLimitUPWithGoalAscending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Less, 10));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OPMetric_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (kc->isAdmissible(id)) {
        model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());

    // change the goal
    kc->changeGoalValue(35);

    // update the model
    EXPECT_FALSE(kc->updateModel(model, *(kc->getLimitOP())));

    // check the model size
    EXPECT_EQ((unsigned) 2, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());

    // check the situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 1, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getWorstOP()));

    // change the goal
    kc->changeGoalValue(50);

    // update the model
    EXPECT_FALSE(kc->updateModel(model, *(kc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 3, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());
}

TEST(KnownCharacteristic, ShiftOPLimitDownWithGoalDescending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Greater, 10));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OPMetric_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (kc->isAdmissible(id)) {
        model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 3, model->S1.size());

    // change the goal
    kc->changeGoalValue(25);

    // update the model
    EXPECT_TRUE(kc->updateModel(model, *(kc->getLimitOP())));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 2, model->S1.size());

    // check the situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 1, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getWorstOP()));

    // change the goal
    kc->changeGoalValue(100);

    // update the model
    EXPECT_TRUE(kc->updateModel(model, *(kc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());
}

TEST(KnownCharacteristic, ShiftOPLimitDownWithGoalAscending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Less, 100));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OPMetric_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (kc->isAdmissible(id)) {
        model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 3, model->S1.size());

    // change the goal
    kc->changeGoalValue(35);

    // update the model
    EXPECT_TRUE(kc->updateModel(model, *(kc->getLimitOP())));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 2, model->S1.size());

    // check the situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 1, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getWorstOP()));

    // change the goal
    kc->changeGoalValue(10);

    // update the model
    EXPECT_TRUE(kc->updateModel(model, *(kc->getLimitOP())));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());

    // check the situation
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 0, *(kc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OPMetric_t) 2, *(kc->getWorstOP()));
}


TEST(KnownCharacteristic, GetDistanceAscending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Less, 30));

    // check the distance
    EXPECT_EQ(10, kc->getDistance(0));
    EXPECT_EQ(0, kc->getDistance(1));
    EXPECT_EQ(10, kc->getDistance(2));
}

TEST(KnownCharacteristic, GetDistanceDiscending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Greater, 30));
    
    // check the distance
    EXPECT_EQ(10, kc->getDistance(0));
    EXPECT_EQ(0, kc->getDistance(1));
    EXPECT_EQ(10, kc->getDistance(2));
}

