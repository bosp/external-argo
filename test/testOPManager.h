#include <memory>
#include <cstdint>
#include <vector>
#include <list>
#include <iostream>
#include "gtest/gtest.h"

#include "argo/monitor/monitor.h"
#include "argo/asrtm/op_manager.h"


TEST(OPManager, NoOPs) {

  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    
    };

  // create the opManager
  ASSERT_THROW(std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList)), std::logic_error);
}


TEST(OPManager, NoRankNoConstraint) {

  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));

  // check the best param
  argo::asrtm::ApplicationParameters param = opManager->getBestApplicationParameters();
  ASSERT_EQ(3, param.find("param")->second);
}

TEST(OPManager, NoConstraintWithRank) {

  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));
  opManager->setSimpleRank("fps", argo::RankObjective::Minimize);

  // check the best param
  argo::asrtm::ApplicationParameters param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);
}


TEST(OPManager, OneAdmissibleOP) {
	// define the op
	argo::asrtm::OperatingPointsList OPList = {
		{ // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "something", 10 },
          },
        }
	};

  // create a known characteristic
  std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("something", OPList, argo::ComparisonFunction::GreaterOrEqual, 10));

  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));
  opManager->setSimpleRank("something", argo::RankObjective::Minimize);

  // add a constraint
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(kc));

  // check the best param
  argo::asrtm::ApplicationParameters param = opManager->getBestApplicationParameters();

  // check the result
  ASSERT_EQ(1, param.find("param")->second);
  
}

TEST(OPManager, OneNotAdmissibleOP) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "something", 10 },
          },
        }
  };

  // create a known characteristic
  std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("something", OPList, argo::ComparisonFunction::Greater, 10));

  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));
  opManager->setSimpleRank("something", argo::RankObjective::Minimize);

  // add a constraint
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(kc));

  // check the best param
  argo::asrtm::ApplicationParameters param = opManager->getBestApplicationParameters();

  // check the result
  ASSERT_EQ(1, param.find("param")->second);
  
}


TEST(OPManager, StressTest) {

  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };
  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

  // create a known characteristic
  std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Greater, 10));

  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));
  opManager->setSimpleRank("fps", argo::RankObjective::Minimize);

  // add a constraint
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(kc));

  // check the best param
  argo::asrtm::ApplicationParameters param = opManager->getBestApplicationParameters();

  // check the result
  ASSERT_EQ(1, param.find("param")->second);

  // increse the goal value
  kc->changeGoalValue(25);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // increse the goal value
  kc->changeGoalValue(35);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(3, param.find("param")->second);

  // increse the goal value
  kc->changeGoalValue(45);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(3, param.find("param")->second);
}



TEST(OPManager, RelaxingTest) {

  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

  // create a known characteristic
  std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Greater, 45));

  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));
  opManager->setSimpleRank("fps", argo::RankObjective::Minimize);

  // add a constraint
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(kc));

  // check the best param
  argo::asrtm::ApplicationParameters param = opManager->getBestApplicationParameters();

  // check the result
  ASSERT_EQ(3, param.find("param")->second);

  // increse the goal value
  kc->changeGoalValue(35);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(3, param.find("param")->second);

  // increse the goal value
  kc->changeGoalValue(25);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // increse the goal value
  kc->changeGoalValue(15);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);
}


TEST(OPManager, UnsatisfiableTwoMetric) {

  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
      { // parametri
        { "param", 1 },
      },
      { // metriche
        { "fps", 20 },
        { "power", 11 },
      },
    },
    { // OP 2
      { // parametri
        { "param", 2 },
      },
      { // metriche
        { "fps", 30 },
        { "power", 14 },
      },
    },
    { // OP 3
      { // parametri
        { "param", 3 },
      },
      { // metriche
        { "fps", 40 },
        { "power", 20 },
      },
    },
    { // OP 4
      { // parametri
        { "param", 4 },
      },
      { // metriche
        { "fps", 50 },
        { "power", 30 },
      },
    },
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

  // create the known characteristics
  std::shared_ptr<argo::asrtm::KnownCharacteristic> fps(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Greater, 25));
  std::shared_ptr<argo::asrtm::KnownCharacteristic> power(new argo::asrtm::KnownCharacteristic("power", OPList, argo::ComparisonFunction::Less, 15));

  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));
  opManager->setSimpleRank("power", argo::RankObjective::Minimize);

  // add the constraints
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(fps));
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(power));

  // check the best param
  argo::asrtm::ApplicationParameters param = opManager->getBestApplicationParameters();

  // check the result
  ASSERT_EQ(2, param.find("param")->second);

  // unsatisfiable for the lower metric
  power->changeGoalValue(14);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // increse the fps's goal value
  fps->changeGoalValue(35);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(3, param.find("param")->second);

  // unsatisfiable even for the upper constraint
  fps->changeGoalValue(60);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // add useless op
  power->changeGoalValue(60);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // unsatisfiable for the lower metric again
  power->changeGoalValue(14);
  fps->changeGoalValue(35);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(3, param.find("param")->second);

  // satisfiable again
  power->changeGoalValue(15);
  fps->changeGoalValue(25);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // totally insatisfiable
  power->changeGoalValue(1);
  fps->changeGoalValue(60);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // totally satisfiable
  power->changeGoalValue(100);
  fps->changeGoalValue(1);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // go back to the initial situation
  power->changeGoalValue(15);
  fps->changeGoalValue(25);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // unsatisfiable/satisfiable in the same step
  power->changeGoalValue(22);
  fps->changeGoalValue(35);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(3, param.find("param")->second);
}


TEST(OPManager, ConstraintHandling) {

  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
      { // parametri
        { "param", 1 },
      },
      { // metriche
        { "fps", 20 },
        { "power", 11 },
        { "quality", 1}
      },
    },
    { // OP 2
      { // parametri
        { "param", 2 },
      },
      { // metriche
        { "fps", 30 },
        { "power", 14 },
        { "quality", 2}
      },
    },
    { // OP 3
      { // parametri
        { "param", 3 },
      },
      { // metriche
        { "fps", 40 },
        { "power", 20 },
        { "quality", 3}
      },
    },
    { // OP 4
      { // parametri
        { "param", 4 },
      },
      { // metriche
        { "fps", 50 },
        { "power", 30 },
        { "quality", 4}
      },
    },
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

  // create the known characteristics
  std::shared_ptr<argo::asrtm::KnownCharacteristic> fps(new argo::asrtm::KnownCharacteristic("fps", OPList, argo::ComparisonFunction::Greater, 25));
  std::shared_ptr<argo::asrtm::KnownCharacteristic> power(new argo::asrtm::KnownCharacteristic("power", OPList, argo::ComparisonFunction::Greater, 40));
  std::shared_ptr<argo::asrtm::KnownCharacteristic> quality(new argo::asrtm::KnownCharacteristic("quality", OPList, argo::ComparisonFunction::Greater, 10));

  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));
  opManager->setSimpleRank("power", argo::RankObjective::Minimize);

  // check the best param
  argo::asrtm::ApplicationParameters param = opManager->getBestApplicationParameters();

  // check the result
  ASSERT_EQ(1, param.find("param")->second);

  // remove a non existent constraint on bottom
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // remove a non existent constraint on top
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // remove a non existent constraint at 0
  opManager->removeConstraintAt(0);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // remove a non existent constraint at 5
  opManager->removeConstraintAt(5);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint bottom
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(fps));

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove the bottom constraint
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint on top
  opManager->addConstraintOnTop(std::dynamic_pointer_cast<argo::asrtm::Constraint>(fps));

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove a contraint on top
  opManager->removeConstraintOnTop();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint before 0
  opManager->addConstraintBefore(0, std::dynamic_pointer_cast<argo::asrtm::Constraint>(fps));

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove a contraint at 0
  opManager->removeConstraintAt(0);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint after 0
  opManager->addConstraintAfter(0, std::dynamic_pointer_cast<argo::asrtm::Constraint>(fps));

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove a contraint at 5 (with one constraint)
  opManager->removeConstraintAt(5);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);


  // add a constraint on top of another
  opManager->addConstraintOnTop(std::dynamic_pointer_cast<argo::asrtm::Constraint>(fps));
  opManager->addConstraintOnTop(std::dynamic_pointer_cast<argo::asrtm::Constraint>(power));

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // remove a contraint on bottom
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // remove a contraint on bottom
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint on bottom of another
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(fps));
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(power));

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // remove a contraint on bottom
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove a contraint on bottom
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint in the middle with before
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(fps));
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(power));
  opManager->addConstraintBefore(1, std::dynamic_pointer_cast<argo::asrtm::Constraint>(quality));

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // remove a contraint in the middle
  opManager->removeConstraintAt(1);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // remove a contraint in the middle
  opManager->removeConstraintAt(1);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove a contraint random
  opManager->removeConstraintAt(100);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint in the middle with after
  opManager->addConstraintBefore(100, std::dynamic_pointer_cast<argo::asrtm::Constraint>(fps));
  opManager->addConstraintAfter(100, std::dynamic_pointer_cast<argo::asrtm::Constraint>(power));
  opManager->addConstraintAfter(0, std::dynamic_pointer_cast<argo::asrtm::Constraint>(quality));

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);
  
}

TEST(OPManager, StaticConstraintChange) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "something", 10 },
          },
        },
    { // OP 2
          { // parameters
            { "param", 2 },
          },
          { // metrics
            { "something", 20 },
          },
        }
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

  // create a known characteristic
  std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("something", OPList, argo::ComparisonFunction::Greater, 10));

  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));
  opManager->setSimpleRank("something", argo::RankObjective::Minimize);

  // add a constraint
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(kc));
  opManager->changeStaticConstraintGoalValue(0, 1);

  // check the best param
  argo::asrtm::ApplicationParameters param = opManager->getBestApplicationParameters();

  // check the result
  ASSERT_EQ(1, param.find("param")->second);
  
}

TEST(OPManager, StaticConstraintIncrement) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "something", 10 },
          },
        },
    { // OP 2
          { // parameters
            { "param", 2 },
          },
          { // metrics
            { "something", 20 },
          },
        }
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

  // create a known characteristic
  std::shared_ptr<argo::asrtm::KnownCharacteristic> kc(new argo::asrtm::KnownCharacteristic("something", OPList, argo::ComparisonFunction::Greater, 10));

  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));
  opManager->setSimpleRank("something", argo::RankObjective::Minimize);

  // add a constraint
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(kc));
  opManager->incrementStaticConstraintGoalValue(0, -1);

  // check the best param
  argo::asrtm::ApplicationParameters param = opManager->getBestApplicationParameters();

  // check the result
  ASSERT_EQ(1, param.find("param")->second);
  
}


TEST(OPManager, StaticConstraintWrongCast) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "something", 10 },
          },
        },
    { // OP 2
          { // parameters
            { "param", 2 },
          },
          { // metrics
            { "something", 20 },
          },
        }
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }


  std::shared_ptr<argo::monitor::DataBuffer<double>> fps_monitor(new argo::monitor::DataBuffer<double>(10));
  std::shared_ptr<argo::monitor::Goal<double>> goal(new argo::monitor::Goal<double>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Less, 11));

  // create the observable characteristic
  std::shared_ptr<argo::asrtm::ObservableCharacteristic<double>> oc(new argo::asrtm::ObservableCharacteristic<double>(goal, OPList, "something"));


  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));
  opManager->setSimpleRank("something", argo::RankObjective::Minimize);

  // add a constraint
  opManager->addConstraintOnBottom(std::dynamic_pointer_cast<argo::asrtm::Constraint>(oc));
  ASSERT_THROW(opManager->changeStaticConstraintGoalValue(0, 1), std::runtime_error);
  
}



TEST(OPManager, ConstraintHandling2) {

  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
      { // parametri
        { "param", 1 },
      },
      { // metriche
        { "fps", 20 },
        { "power", 11 },
        { "quality", 1}
      },
    },
    { // OP 2
      { // parametri
        { "param", 2 },
      },
      { // metriche
        { "fps", 30 },
        { "power", 14 },
        { "quality", 2}
      },
    },
    { // OP 3
      { // parametri
        { "param", 3 },
      },
      { // metriche
        { "fps", 40 },
        { "power", 20 },
        { "quality", 3}
      },
    },
    { // OP 4
      { // parametri
        { "param", 4 },
      },
      { // metriche
        { "fps", 50 },
        { "power", 30 },
        { "quality", 4}
      },
    },
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

  std::shared_ptr<argo::monitor::DataBuffer<double>> fps_monitor(new argo::monitor::DataBuffer<double>(10));
  std::shared_ptr<argo::monitor::Goal<double>> goal(new argo::monitor::Goal<double>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 25));

  // create the opManager
  std::shared_ptr<argo::asrtm::OPManager> opManager(new argo::asrtm::OPManager(OPList));
  opManager->setSimpleRank("power", argo::RankObjective::Minimize);

  // check the best param
  argo::asrtm::ApplicationParameters param = opManager->getBestApplicationParameters();

  // check the result
  ASSERT_EQ(1, param.find("param")->second);

  // remove a non existent constraint on bottom
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // remove a non existent constraint on top
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // remove a non existent constraint at 0
  opManager->removeConstraintAt(0);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // remove a non existent constraint at 5
  opManager->removeConstraintAt(5);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint bottom
  opManager->addDynamicConstraintOnBottom(goal, "fps");

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove the bottom constraint
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint on top
  opManager->addDynamicConstraintOnTop(goal, "fps");

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove a contraint on top
  opManager->removeConstraintOnTop();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint before 0
  opManager->addDynamicConstraintBefore(0, goal, "fps");

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove a contraint at 0
  opManager->removeConstraintAt(0);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint after 0
  opManager->addDynamicConstraintAfter(0, goal, "fps");

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove a contraint at 5 (with one constraint)
  opManager->removeConstraintAt(5);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);


  // add a constraint on top of another
  opManager->addDynamicConstraintOnTop(goal, "fps");
  opManager->addStaticConstraintOnTop("power", argo::ComparisonFunction::Greater, 40);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // remove a contraint on bottom
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // remove a contraint on bottom
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint on bottom of another
  opManager->addDynamicConstraintOnBottom(goal, "fps");
  opManager->addStaticConstraintOnBottom("power", argo::ComparisonFunction::Greater, 40);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // remove a contraint on bottom
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove a contraint on bottom
  opManager->removeConstraintOnBottom();

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint in the middle with before
  opManager->addDynamicConstraintOnBottom(goal, "fps");
  opManager->addStaticConstraintOnBottom("power", argo::ComparisonFunction::Greater, 40);
  opManager->addStaticConstraintBefore(1, "quality", argo::ComparisonFunction::Greater, 10);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // remove a contraint in the middle
  opManager->removeConstraintAt(1);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);

  // remove a contraint in the middle
  opManager->removeConstraintAt(1);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(2, param.find("param")->second);

  // remove a contraint random
  opManager->removeConstraintAt(100);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(1, param.find("param")->second);

  // add a constraint in the middle with after
  opManager->addDynamicConstraintBefore(100, goal, "fps");
  opManager->addStaticConstraintAfter(100, "power", argo::ComparisonFunction::Greater, 40);
  opManager->addStaticConstraintAfter(0, "quality", argo::ComparisonFunction::Greater, 10);

  // check the result
  param = opManager->getBestApplicationParameters();
  ASSERT_EQ(4, param.find("param")->second);
  
}
