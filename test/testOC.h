#include <memory>
#include <cstdint>
#include <vector>
#include <list>
#include "gtest/gtest.h"

#include "argo/asrtm/observable_characteristic.h"
#include "argo/monitor/goal.h"
#include "argo/monitor/data_buffer.h"
#include "argo/asrtm/model.h"
#include "argo/config.h"


int test_oc = 0;

void my_callback_oc() {
  test_oc = 4;
}


TEST(ObservableCharacteristic, ZeroOP) {
	// define the op
	argo::asrtm::OperatingPointsList OPList = {
	};

    std::shared_ptr<argo::monitor::DataBuffer<int>> something_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(something_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 25));

    // create the observable characteristic
    ASSERT_THROW({std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "something"));}, std::logic_error);
}

TEST(ObservableCharacteristic, SingleOP) {
	// define the op
	argo::asrtm::OperatingPointsList OPList = {
		{ // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "foo", 10 },
          },
        }
	};

    argo::asrtm::OperatingPointID_t counter_id = 0;
    for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
        op->id = counter_id;
        counter_id++;
    }

    std::shared_ptr<argo::monitor::DataBuffer<int>> something_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(something_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 10));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "foo"));


    // check the best op
    EXPECT_EQ((uint32_t) 0, *(oc->getBestOP()));

    // check the limit op
    EXPECT_EQ((uint32_t) 0, *(oc->getLimitOP()));

    // check the worst op
    EXPECT_EQ((uint32_t) 0, *(oc->getWorstOP()));
}

TEST(ObservableCharacteristic, RelaxableConstraintCall) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "foo", 10 },
          },
        }
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    std::shared_ptr<argo::monitor::DataBuffer<int>> something_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(something_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 10));
    goal->setCallbackFunction(my_callback_oc);
    something_monitor->push(10);

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "foo", true));


    test_oc = 0;
    oc->checkRelaxable(0);

    // check calling
    EXPECT_EQ(4, test_oc);
}

TEST(ObservableCharacteristic, RelaxableConstraintNoCall1) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "foo", 10 },
          },
        }
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }


    std::shared_ptr<argo::monitor::DataBuffer<int>> something_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(something_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 10));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "foo"));


    test_oc = 0;
    oc->checkRelaxable(0);

    // check calling
    EXPECT_EQ(0, test_oc);
}

TEST(ObservableCharacteristic, RelaxableConstraintNoCall2) {
  // define the op
  argo::asrtm::OperatingPointsList OPList = {
    { // OP 1
          { // parameters
            { "param", 1 },
          },
          { // metrics
            { "foo", 10 },
          },
        }
  };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }
  std::shared_ptr<argo::monitor::DataBuffer<int>> something_monitor(new argo::monitor::DataBuffer<int>());
  std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(something_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 9));

  // create the observable characteristic
  std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "foo"));

  something_monitor->push(10);


    test_oc = 0;
    oc->checkRelaxable(0);

    // check calling
    EXPECT_EQ(0, test_oc);
}



TEST(ObservableCharacteristic, DescendingOrdering) {

	// define the ops
	argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 10 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 4
        { // parameters
          { "param", 4 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
      { // OP 5
        { // parameters
          { "param", 5 },
        },
        { // metrics
          { "fps", 50 },
        },
      },
    };

    argo::asrtm::OperatingPointID_t counter_id = 0;
    for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
        op->id = counter_id;
        counter_id++;
    }


    std::shared_ptr<argo::monitor::DataBuffer<int>> something_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(something_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 25));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));
    
    // check the best op
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 4, *(oc->getBestOP()));

    // check the limit op
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getLimitOP()));

    // check the worst op
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));
}


TEST(ObservableCharacteristic, AscendingOrdering) {
// define the ops
	argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "power", 10 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "power", 20 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "power", 30 },
        },
      },
      { // OP 4
        { // parameters
          { "param", 4 },
        },
        { // metrics
          { "power", 40 },
        },
      },
      { // OP 5
        { // parameters
          { "param", 5 },
        },
        { // metrics
          { "power", 50 },
        },
      },
    };

    argo::asrtm::OperatingPointID_t counter_id = 0;
    for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
        op->id = counter_id;
        counter_id++;
    }



    std::shared_ptr<argo::monitor::DataBuffer<int>> something_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal( new argo::monitor::Goal<int>(something_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Less, 30));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "power"));
    
    // check the best op
    EXPECT_EQ((uint32_t) 0, *(oc->getBestOP()));

    // check the limit op
    EXPECT_EQ((uint32_t) 1, *(oc->getLimitOP()));

    // check the worst op
    EXPECT_EQ((uint32_t) 4, *(oc->getWorstOP()));
}

TEST(ObservableCharacteristic, AdmissibleGreater) {

	// define the ops
	argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

    argo::asrtm::OperatingPointID_t counter_id = 0;
    for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
        op->id = counter_id;
        counter_id++;
    }


    std::shared_ptr<argo::monitor::DataBuffer<int>> something_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal( new argo::monitor::Goal<int>(something_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 30));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));
    
    // check the validity
    EXPECT_FALSE(oc->isAdmissible(0));
    EXPECT_FALSE(oc->isAdmissible(1));
    EXPECT_TRUE(oc->isAdmissible(2));
}

TEST(ObservableCharacteristic, AdmissibleGreaterOrEqual) {

	// define the ops
	argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

    argo::asrtm::OperatingPointID_t counter_id = 0;
    for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
        op->id = counter_id;
        counter_id++;
    }


    std::shared_ptr<argo::monitor::DataBuffer<int>> something_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(something_monitor, argo::DataFunction::Average, argo::ComparisonFunction::GreaterOrEqual, 30));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));
    
    // check the validity
    EXPECT_FALSE(oc->isAdmissible(0));
    EXPECT_TRUE(oc->isAdmissible(1));
    EXPECT_TRUE(oc->isAdmissible(2));
}

TEST(ObservableCharacteristic, AdmissibleLess) {

	// define the ops
	argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "power", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "power", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "power", 40 },
        },
      },
    };

    argo::asrtm::OperatingPointID_t counter_id = 0;
    for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
        op->id = counter_id;
        counter_id++;
    }

    std::shared_ptr<argo::monitor::DataBuffer<int>> something_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(something_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Less, 30));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "power"));
    
    // check the validity
    EXPECT_TRUE(oc->isAdmissible(0));
    EXPECT_FALSE(oc->isAdmissible(1));
    EXPECT_FALSE(oc->isAdmissible(2));
}

TEST(ObservableCharacteristic, AdmissibleLessOrEqual) {

	// define the ops
	argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "power", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "power", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "power", 40 },
        },
      },
    };

    argo::asrtm::OperatingPointID_t counter_id = 0;
    for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
        op->id = counter_id;
        counter_id++;
    }


    std::shared_ptr<argo::monitor::DataBuffer<int>> something_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(something_monitor, argo::DataFunction::Average, argo::ComparisonFunction::LessOrEqual, 30));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "power"));
    
    // check the validity
    EXPECT_TRUE(oc->isAdmissible(0));
    EXPECT_TRUE(oc->isAdmissible(1));
    EXPECT_FALSE(oc->isAdmissible(2));
}

TEST(ObservableCharacteristic, ShiftOPLimitUPWithGoalDescending) {

	// define the ops
	argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

    argo::asrtm::OperatingPointID_t counter_id = 0;
    for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
        op->id = counter_id;
        counter_id++;
    }

    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 100));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OperatingPointID_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (oc->isAdmissible(id)) {
      	model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());

    // change the goal
    goal->setGoalValue(25);

    // update the model
    EXPECT_FALSE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the model size
    EXPECT_EQ((unsigned) 2, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 1, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    // change the goal
    goal->setGoalValue(0);

    // update the model
    EXPECT_FALSE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 3, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());
}

TEST(ObservableCharacteristic, ShiftOPLimitUPWithGoalAscending) {

	// define the ops
	argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

    argo::asrtm::OperatingPointID_t counter_id = 0;
    for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
        op->id = counter_id;
        counter_id++;
    }


    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Less, 10));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OperatingPointID_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (oc->isAdmissible(id)) {
      	model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());

    // change the goal
    goal->setGoalValue(35);

    // update the model
    EXPECT_FALSE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the model size
    EXPECT_EQ((unsigned) 2, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 1, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));

    // change the goal
    goal->setGoalValue(50);

    // update the model
    EXPECT_FALSE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 3, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());
}

TEST(ObservableCharacteristic, ShiftOPLimitDownWithGoalDescending) {

	// define the ops
	argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

    argo::asrtm::OperatingPointID_t counter_id = 0;
    for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
        op->id = counter_id;
        counter_id++;
    }

    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>());
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 10));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OperatingPointID_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (oc->isAdmissible(id)) {
      	model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 3, model->S1.size());

    // change the goal
    goal->setGoalValue(25);

    // update the model
    EXPECT_TRUE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 2, model->S1.size());

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 1, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    // change the goal
    goal->setGoalValue(100);

    // update the model
    EXPECT_TRUE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());
}

TEST(ObservableCharacteristic, ShiftOPLimitDownWithGoalAscending) {

	// define the ops
	argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

    argo::asrtm::OperatingPointID_t counter_id = 0;
    for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
        op->id = counter_id;
        counter_id++;
    }


    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>(10));
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Less, 100));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    uint32_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (oc->isAdmissible(id)) {
      	model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 3, model->S1.size());

    // change the goal
    goal->setGoalValue(35);

    // update the model
    EXPECT_TRUE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 2, model->S1.size());

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 1, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));

    // change the goal
    goal->setGoalValue(10);

    // update the model
    EXPECT_TRUE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the model size
    EXPECT_EQ((unsigned) 0, model->S2.size());
    EXPECT_EQ((unsigned) 0, model->S1.size());

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));
}

TEST(ObservableCharacteristic, GetDistanceAscending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }


    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>(10));
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Less, 30));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));
    
    // check the distance
    EXPECT_EQ(10, oc->getDistance(0));
    EXPECT_EQ(0, oc->getDistance(1));
    EXPECT_EQ(10, oc->getDistance(2));
}


TEST(ObservableCharacteristic, GetDistanceDiscending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };


  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>(10));
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 30));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));
    
    // check the distance
    EXPECT_EQ(10, oc->getDistance(0));
    EXPECT_EQ(0, oc->getDistance(1));
    EXPECT_EQ(10, oc->getDistance(2));
}


TEST(ObservableCharacteristic, ValidityWindowOne) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };


  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }
    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>(10));
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Less, 30));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OperatingPointID_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (oc->isAdmissible(id)) {
        model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation with empty window
    EXPECT_EQ(20, oc->getOPValue(0));
    EXPECT_EQ(30, oc->getOPValue(1));
    EXPECT_EQ(40, oc->getOPValue(2));

    // add avalue in the window
    fps_monitor->push(10);

    // update the model
    EXPECT_FALSE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the new situation with empty window
    EXPECT_EQ(10, oc->getOPValue(0));
    EXPECT_EQ(15, oc->getOPValue(1));
    EXPECT_EQ(20, oc->getOPValue(2));
}


TEST(ObservableCharacteristic, ValidityWindowThree) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>(10));
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Less, 30));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));

    // set the validity window at three element
    oc->setElementNumber(3);

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OperatingPointID_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (oc->isAdmissible(id)) {
        model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation with empty window
    EXPECT_EQ(20, oc->getOPValue(0));
    EXPECT_EQ(30, oc->getOPValue(1));
    EXPECT_EQ(40, oc->getOPValue(2));

    // add avalue in the window
    fps_monitor->push(40);

    // update the model
    EXPECT_FALSE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the new situation with empty window
    EXPECT_EQ(20, oc->getOPValue(0));
    EXPECT_EQ(30, oc->getOPValue(1));
    EXPECT_EQ(40, oc->getOPValue(2));

    // add avalue in the window
    fps_monitor->push(40);

    // update the model
    EXPECT_FALSE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the new situation with empty window
    EXPECT_EQ(20, oc->getOPValue(0));
    EXPECT_EQ(30, oc->getOPValue(1));
    EXPECT_EQ(40, oc->getOPValue(2));

    // add avalue in the window
    fps_monitor->push(40);

    // update the model
    EXPECT_TRUE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the new situation with empty window
    EXPECT_EQ(40, oc->getOPValue(0));
    EXPECT_EQ(60, oc->getOPValue(1));
    EXPECT_EQ(80, oc->getOPValue(2));
}

TEST(ObservableCharacteristic, ShiftOPLimitUPWithObservationDescending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>(10));
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Greater, 100));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OperatingPointID_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (oc->isAdmissible(id)) {
        model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    // add avalue in the window
    fps_monitor->push(150);

    // update the model
    EXPECT_FALSE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 1, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    // add avalue in the window
    fps_monitor->push(300);

    // update the model
    EXPECT_FALSE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    
}

TEST(ObservableCharacteristic, ShiftOPLimitUPWithObservationAscending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>(10));
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::Less, 20));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OperatingPointID_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (oc->isAdmissible(id)) {
        model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));

    // add an element
    fps_monitor->push(10);

    // update the model
    EXPECT_FALSE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 1, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));

    // change the goal
    fps_monitor->push(10);

    // update the model
    EXPECT_FALSE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));
}

TEST(ObservableCharacteristic, ShiftOPLimitDownWithObservationDescending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }


    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>(10));
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::GreaterOrEqual, 15));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OperatingPointID_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (oc->isAdmissible(id)) {
        model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    // add avalue in the window
    fps_monitor->push(10);

    // update the model
    EXPECT_TRUE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 1, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    // add avalue in the window
    fps_monitor->push(2);

    // update the model
    EXPECT_TRUE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getWorstOP()));

    
}

TEST(ObservableCharacteristic, ShiftOPLimitDownWithObservationAscending) {

  // define the ops
  argo::asrtm::OperatingPointsList OPList = {
      { // OP 1
        { // parameters
          { "param", 1 },
        },
        { // metrics
          { "fps", 20 },
        },
      },
      { // OP 2
        { // parameters
          { "param", 2 },
        },
        { // metrics
          { "fps", 30 },
        },
      },
      { // OP 3
        { // parameters
          { "param", 3 },
        },
        { // metrics
          { "fps", 40 },
        },
      },
    };

  argo::asrtm::OperatingPointID_t counter_id = 0;
  for(argo::asrtm::OperatingPointsList::iterator op = OPList.begin(); op != OPList.end(); ++op) {
      op->id = counter_id;
      counter_id++;
  }

    std::shared_ptr<argo::monitor::DataBuffer<int>> fps_monitor(new argo::monitor::DataBuffer<int>(10));
    std::shared_ptr<argo::monitor::Goal<int>> goal(new argo::monitor::Goal<int>(fps_monitor, argo::DataFunction::Average, argo::ComparisonFunction::LessOrEqual, 40));

    // create the observable characteristic
    std::shared_ptr<argo::asrtm::ObservableCharacteristic<int>> oc(new argo::asrtm::ObservableCharacteristic<int>(goal, OPList, "fps"));

    // create the model
    std::shared_ptr<argo::asrtm::Model> model(new argo::asrtm::Model(OPList.size()));

    // populate the model
    argo::asrtm::OperatingPointID_t id = 0;
    for(argo::asrtm::OperatingPointsList::iterator it = OPList.begin(); it != OPList.end(); ++it) {
      if (oc->isAdmissible(id)) {
        model->addOperatingPointToS1(id);
      }
      id++;
    }
    
    // check the initial situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));

    // add an element
    fps_monitor->push(50);

    // update the model
    EXPECT_TRUE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 1, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));

    // change the goal
    fps_monitor->push(100);

    // update the model
    EXPECT_TRUE(oc->updateModel(model, *(oc->getLimitOP())));

    // check the situation
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getBestOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 0, *(oc->getLimitOP()));
    EXPECT_EQ((argo::asrtm::OperatingPointID_t) 2, *(oc->getWorstOP()));
}
