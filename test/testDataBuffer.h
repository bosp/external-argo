#include <memory>
#include <cstdint>
#include <vector>
#include <list>
#include "gtest/gtest.h"

#include "argo/monitor/data_buffer.h"


TEST(DataBufferTest, getOnNoData) {


    std::shared_ptr<argo::monitor::DataBuffer<int>> p (new argo::monitor::DataBuffer<int>);

    EXPECT_EQ(0, p->get(argo::DataFunction::Average));
    EXPECT_EQ(0, p->get(argo::DataFunction::Max));
    EXPECT_EQ(0, p->get(argo::DataFunction::Min));
    EXPECT_EQ(0, p->get(argo::DataFunction::Variance));

}

TEST(DataBufferTest, utilityFunctions) {


    std::shared_ptr<argo::monitor::DataBuffer<int>> p (new argo::monitor::DataBuffer<int>(2));

    EXPECT_EQ((unsigned) 2, p->getBufferMaxSize());
    EXPECT_EQ(0, p->getLastElement());
    EXPECT_EQ(0, p->size());
    EXPECT_FALSE(p->full());
    EXPECT_TRUE(p->empty());

    p->push(4);

    EXPECT_EQ((unsigned) 2, p->getBufferMaxSize());
    EXPECT_EQ(4, p->getLastElement());
    EXPECT_EQ(1, p->size());
    EXPECT_FALSE(p->full());
    EXPECT_FALSE(p->empty());


    p->push(5);

    EXPECT_EQ((unsigned) 2, p->getBufferMaxSize());
    EXPECT_EQ(5, p->getLastElement());
    EXPECT_EQ(2, p->size());
    EXPECT_TRUE(p->full());
    EXPECT_FALSE(p->empty());

}



TEST(DataBufferTest, getAverageInt) {


    std::shared_ptr<argo::monitor::DataBuffer<int>> p (new argo::monitor::DataBuffer<int>(3));

    p->push(-3);
    p->push(0);
    p->push(3);

    EXPECT_EQ(0, p->get(argo::DataFunction::Average));

}


TEST(DataBufferTest, getAverageUnsignedInt) {


    std::shared_ptr<argo::monitor::DataBuffer<unsigned int>> p (new argo::monitor::DataBuffer<unsigned int>(3));

    p->push(1);
    p->push(2);
    p->push(3);

    EXPECT_EQ(2, p->get(argo::DataFunction::Average));

}


TEST(DataBufferTest, getAverageFloat) {


    std::shared_ptr<argo::monitor::DataBuffer<float>> p (new argo::monitor::DataBuffer<float>(3));

    p->push(0.1);
    p->push(0.2);
    p->push(0.3);

    EXPECT_EQ(0.2f, p->get(argo::DataFunction::Average));

}

TEST(DataBufferTest, getVariance) {


    std::shared_ptr<argo::monitor::DataBuffer<float>>  p (new argo::monitor::DataBuffer<float>(3));

    p->push(-1);
    p->push(0);
    p->push(1);

    EXPECT_EQ(1.0f, p->get(argo::DataFunction::Variance));

}


TEST(DataBufferTest, getVarianceUnsigned) {


    std::shared_ptr<argo::monitor::DataBuffer<unsigned int>> p (new argo::monitor::DataBuffer<unsigned>(3));

    p->push(1);
    p->push(2);
    p->push(3);

    EXPECT_EQ(1, p->get(argo::DataFunction::Variance));

}


TEST(DataBufferTest, getMinMax) {


    std::shared_ptr<argo::monitor::DataBuffer<int>> p (new argo::monitor::DataBuffer<int>(3));

    p->push(-1);
    p->push(-2);
    p->push(-3);

    EXPECT_EQ(-1, p->get(argo::DataFunction::Max));
    EXPECT_EQ(-3, p->get(argo::DataFunction::Min));

    p->clear();
    p->push(1);
    p->push(2);
    p->push(3);

    EXPECT_EQ(3, p->get(argo::DataFunction::Max));
    EXPECT_EQ(1, p->get(argo::DataFunction::Min));

    p->clear();
    p->push(1);

    EXPECT_EQ(1, p->get(argo::DataFunction::Max));
    EXPECT_EQ(1, p->get(argo::DataFunction::Min));

    p->clear();
    p->push(1);
    p->push(1);
    p->push(1);


    EXPECT_EQ(1, p->get(argo::DataFunction::Max));
    EXPECT_EQ(1, p->get(argo::DataFunction::Min));

}

TEST(DataBufferTest, circularity) {

    std::shared_ptr<argo::monitor::DataBuffer<int>> p (new argo::monitor::DataBuffer<int>(2));

    p->push(0);
    p->push(0);
    EXPECT_EQ(0, p->get(argo::DataFunction::Average));

    p->push(1);
    p->push(1);
    EXPECT_EQ(1, p->get(argo::DataFunction::Average));
}
