# README
This repository contains the ARGO framework

## Summary of the framework

The ARGO framework provides to an application the ability to dynamically adapt, in order to face changes in the execution enviroment and peak of workload. The framework exploits the informations gathered during a Design Space Exploration in order to guide the selection of the best application parameters. The application can define a set constraints on its performance and a rank function that express how good is an Operating Point for the application. At Run-Time the framework senses the execution enviroments, by means of run-time monitors, and automatically selects the best parameters that fit the observed situation.


### Compiling instructions
The building system is based on CMake. Assuming you are in the path path/to/external-argo, the default procedure is as follow:
~~~
:::bash
$ mkdir build
$ cd build
$ cmake -DCMAKE_INSTALL_PREFIX:PATH=<path> ..
$ make
$ make install
~~~
In order to build the framework is needed gcc > 4.6. The framework is meant to be used in unix enviroment. The Application-Specific Run-Time Manger is written in standard c++, however several monitors implementation parse the /proc metafiles.

#### Building option
The default configuration builds and installs only the framework as a static library ( togheter with header files). However is possible to change this behaviour using the cmake configuration options as follow:

| Option name              |  Values [default]  | Description                               |
|--------------------------|--------------------|-------------------------------------------|
| LIB_STATIC               |   ON , [OFF]       | Build a shared library                    |
| LIB_DYNAMIC              |  [ON], OFF         | Build a static library                    |
| TEST                     |   ON , [OFF]       | Build a test application                  |
| BENCHMARK                |   ON , [OFF]       | Build a benchmark of the framework        |


### Contribution guidelines
Clone the repository and send pull requests, any contribution is welcome.

### Who do I talk to?
Contact: davide [dot] gadioli [at] polimi [dot] it

Organization: Politecnico di Milano, Italy