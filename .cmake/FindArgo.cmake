
# Locate Argo framework include paths and libraries

# This module defines
# ARGO_INCLUDE_DIR, where to find asrtm.h, etc.
# ARGO_LIBRARIES, the libraries to link against to use the Argo framework.
# ARGO_LINK_DIRECTORIES, where to find the libraries
# ARGO_FOUND, If false, don't try to use RTLib.

find_path(ARGO_INCLUDE_DIR argo/monitor/monitor.h)
find_path(ARGO_LINK_DIRECTORIES libargo_monitor.so
          PATH_SUFFIXES lib )

set(ARGO_FOUND 0)
if (ARGO_INCLUDE_DIR)
  if (ARGO_LINK_DIRECTORIES)
    set(ARGO_FOUND 1)
    set(ARGO_LIBRARIES argo_monitor argo_asrtm)
    message(STATUS "Found Argo shared libraries: ${ARGO_LINK_DIRECTORIES}")
  else (ARGO_LINK_DIRECTORIES)
    find_path(ARGO_LINK_DIRECTORIES argo_monitorStatic.a
              PATH_SUFFIXES lib )
    if (ARGO_LINK_DIRECTORIES)
      set(ARGO_FOUND 1)
      message(STATUS "Found Argo static libraries: ${ARGO_LINK_DIRECTORIES}")
      set(ARGO_LIBRARIES argo_monitorStatic argo_asrtmStatic)
    else( ARGO_LINK_DIRECTORIES )
      message(FATAL_ERROR "Argo framework libraries NOT FOUND!")
    endif (ARGO_LINK_DIRECTORIES)
  endif (ARGO_LINK_DIRECTORIES )
else (ARGO_INCLUDE_DIR)
  message(FATAL_ERROR "Argo headers NOT FOUND!")
endif (ARGO_INCLUDE_DIR)

mark_as_advanced(
  ARGO_INCLUDE_DIR
  ARGO_LIBRARIES
  ARGO_LINK_DIRECTORIES
  ARGO_FOUND
)
