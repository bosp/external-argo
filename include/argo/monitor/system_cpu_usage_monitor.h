/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARGO_SYSTEM_CPU_USAGE_MONITOR_H
#define ARGO_SYSTEM_CPU_USAGE_MONITOR_H

#include <memory>


#include "argo/config.h"
#include "argo/monitor/data_buffer.h"
#include "argo/monitor/goal.h"



/**
 * @brief  The main namespace that wrap the framework
 */
namespace argo {
	
/**
 * @brief  The sub namespace that wrap the Monitor infrastructure
 * 
 */	
namespace monitor {



/**
 * @brief  The System CPU usage monitor
 *
 * @details
 * This class rappresent a monitor that observe the percentage of time that the whole system
 * has spent in user or system time over the observation period.
 * The measure is used parsing the /proc/stat metafile
 */
class SystemCpuUsageMonitor: public DataBuffer<CpuUsage_t> {
public:
	
	/**
	 * @brief  Default constructor
	 * 
	 * @param window_size The dimension of the obseravation window
	 * 
	 */
    SystemCpuUsageMonitor(WindowSize window_size = ARGO_DEFAULT_BUFFER_SIZE);

	/**
	 * @brief  Start the observation
	 * 
	 */
    void start();


	/**
	 * @brief  Stop the observation and push the new data in the buffer
	 * 
	 */
    void stop();


private:

    Time_t busy_time;

    Time_t total_time;


    bool started;
};




typedef std::shared_ptr<argo::monitor::SystemCpuUsageMonitor> SystemCpuUsageMonitorPtr;


typedef argo::monitor::Goal<argo::monitor::CpuUsage_t> SystemCpuUsageGoal;
typedef std::shared_ptr<SystemCpuUsageGoal> SystemCpuUsageGoalPtr;


} // namespace monitor

} // namespace argo







#endif // ARGO_SYSTEM_CPU_USAGE_MONITOR_H
