/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARGO_MONITOR_TIME_MONITOR_H_
#define ARGO_MONITOR_TIME_MONITOR_H_

#include <chrono>
#include <memory>
#include <cstdint>


#include "argo/monitor/data_buffer.h"
#include "argo/config.h"
#include "argo/monitor/goal.h"


/**
 * @brief  The main namespace that wrap the framework
 */
namespace argo {
	
/**
 * @brief  The sub namespace that wrap the Monitor infrastructure
 * 
 */
namespace monitor {


// default milliseconds
enum class TimeMeasure: uint8_t {
    Nanoseconds,
    Microseconds,
    Milliseconds,
    Seconds
};



/**
 * @brief  The time monitor
 *
 * @details
 * This use the std::chrono interface to gather the execution time
 * using a steady clock, if available. Otherwise it use the monotonic_clock
 */
class TimeMonitor: public DataBuffer<Time_t> {
public:
	
	/**
	 * @brief  Default constructor
	 * 
	 * @param window_size The dimension of the obseravation window
	 * 
	 * @details
	 * The default measure is in milliseconds
	 * 
	 */
    TimeMonitor(WindowSize window_size = ARGO_DEFAULT_BUFFER_SIZE);

	/**
	 * @brief  Generalized constructor
	 * 
	 * @param time_measure The measure unit
	 * 
	 * @param window_size The dimension of the obseravation window
	 * 
	 */
    TimeMonitor(TimeMeasure time_measure, WindowSize window_size = ARGO_DEFAULT_BUFFER_SIZE );

	/**
	 * @brief  Start the observation
	 * 
	 */
    void start();


	/**
	 * @brief  Stop the observation and push the new data in the buffer
	 * 
	 */
    void stop();

private:

    // initial time measure
    std::chrono::steady_clock::time_point tStart;


    // flag for the measuring state
    bool started;

    // time measure converter
    std::function<Time_t(std::chrono::steady_clock::time_point,std::chrono::steady_clock::time_point)> time_extractor;


};


typedef std::shared_ptr<argo::monitor::TimeMonitor> TimeMonitorPtr;


typedef argo::monitor::Goal<argo::monitor::Time_t> TimeGoal;
typedef std::shared_ptr<TimeGoal> TimeGoalPtr;



} // namespace monitor

} // namespace argo

#endif /* ARGO_MONITOR_TIME_MONITOR_H_ */
