#ifndef ARGO_MONITOR_MONITOR_H
#define ARGO_MONITOR_MONITOR_H

#include<map>
#include<string>
#include<vector>
#include<list>
#include<mutex>
#include<stdexcept>
#include<iostream>



#include "argo/monitor/throughput_monitor.h"
#include "argo/monitor/memory_monitor.h"
#include "argo/monitor/process_cpu_usage_monitor.h"
#include "argo/monitor/system_cpu_usage_monitor.h"
#include "argo/monitor/throughput_monitor.h"
#include "argo/monitor/time_monitor.h"
#include "argo/monitor/goal.h"



// -------------------------------------------------------  General macros utility

#define ARGO_DEF_MONITOR_STRUCTURE_C( name, structure )\
    class name {\
    public:\
    structure\
    };

#define ARGO_MONITOR( name, type )\
    static std::shared_ptr<type> name ;


#define ARGO_GOAL( name, type )\
    static std::shared_ptr<argo::monitor::Goal<type>> name ;


#define ARGO_INIT_MONITOR_C( structure_name, name, type, ...)\
    std::shared_ptr<type> structure_name::name = std::shared_ptr<type>(new type( __VA_ARGS__ ));


#define ARGO_INIT_GOAL_C(structure_name, name, type, ...)\
    std::shared_ptr<argo::monitor::Goal<type>> structure_name::name = std::shared_ptr<argo::monitor::Goal<type>>(new argo::monitor::Goal<type>( __VA_ARGS__ ));





// default class name
#define ARGO_MONITOR_STRUCTURE_NAME_DEFAULT MyMonitors


#define ARGO_DEF_MONITOR_STRUCTURE( structure )\
    ARGO_DEF_MONITOR_STRUCTURE_C(ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, structure)



// --------------------------------------------------------- Throughput macros
#define ARGO_INIT_THROUGHPUT_MONITOR(name, ...)\
    ARGO_INIT_MONITOR_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, argo::monitor::ThroughputMonitor, __VA_ARGS__)


#define ARGO_INIT_THROUGHPUT_GOAL(name, ...)\
    ARGO_INIT_GOAL_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, argo::monitor::Throughput_t, __VA_ARGS__)



// --------------------------------------------------------- Time macros
#define ARGO_INIT_TIME_MONITOR(name, ...)\
    ARGO_INIT_MONITOR_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, argo::monitor::TimeMonitor, __VA_ARGS__)


#define ARGO_INIT_TIME_GOAL(name, ...)\
    ARGO_INIT_GOAL_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, argo::monitor::Time_t, __VA_ARGS__)



// --------------------------------------------------------- Memory macros
#define ARGO_INIT_MEMORY_MONITOR(name, ...)\
    ARGO_INIT_MONITOR_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, argo::monitor::MemoryMonitor, __VA_ARGS__)


#define ARGO_INIT_MEMORY_GOAL(name, ...)\
    ARGO_INIT_GOAL_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, argo::monitor::Memory_t, __VA_ARGS__)



// --------------------------------------------------------- Process cpu usage
#define ARGO_INIT_PROCESS_USAGE_MONITOR(name, ...)\
    ARGO_INIT_MONITOR_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, argo::monitor::ProcessCpuUsageMonitor, __VA_ARGS__)


#define ARGO_INIT_PROCESS_USAGE_GOAL(name, ...)\
    ARGO_INIT_GOAL_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, argo::monitor::CpuUsage_t, __VA_ARGS__)



// --------------------------------------------------------- System cpu usage
#define ARGO_INIT_SYSTEM_USAGE_MONITOR(name, ...)\
    ARGO_INIT_MONITOR_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, argo::monitor::SystemCpuUsageMonitor, __VA_ARGS__)


#define ARGO_INIT_SYSTEM_USAGE_GOAL(name, ...)\
    ARGO_INIT_GOAL_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, argo::monitor::CpuUsage_t, __VA_ARGS__)


//---------------------------------------------------------- Custom
#define ARGO_INIT_CUSTOM_MONITOR(name, type, ...)\
    ARGO_INIT_MONITOR_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, type, __VA_ARGS__)


#define ARGO_INIT_CUSTOM_GOAL(name, type, ...)\
    ARGO_INIT_GOAL_C( ARGO_MONITOR_STRUCTURE_NAME_DEFAULT, name, type, __VA_ARGS__)



#endif // ARGO_MONITOR_MONITOR_H
