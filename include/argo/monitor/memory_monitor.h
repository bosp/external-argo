/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARGO_MONITOR_MEMORY_MONITOR_H_
#define ARGO_MONITOR_MEMORY_MONITOR_H_

#include <cstdint>

#include "argo/config.h"
#include "argo/monitor/data_buffer.h"
#include "argo/monitor/goal.h"



/**
 * @brief  The main namespace that wrap the framework
 */
namespace argo {
	
/**
 * @brief  The sub namespace that wrap the Monitor infrastructure
 * 
 */	
namespace monitor {




/**
 * @brief  The Memory Monitor
 * 
 * @template The type of the monitor
 *
 * @details
 * This class rappresent a monitor that observe the memory used by the application.
 * Moreover it provide a methods that get the peam virtual memory used
 */
class MemoryMonitor: public DataBuffer<Memory_t> {
public:

	/**
	 * @brief  The Memory Monitor constructor
	 * 
	 * @param window_size The dimension of the obseravation window
	 *
	 */
    MemoryMonitor(WindowSize window_size = ARGO_DEFAULT_BUFFER_SIZE);

	/**
	 * @brief  read the memory usage of the application
	 *
	 * @details
	 * The measure of the memory is extracted by parsing the /proc/self/statm
	 * metafile. For this reason the measure doesn't require a start()/stop().
	 * The measure is pushed inside the data buffer
	 */
    void extractMemoryUsage();

	/**
	 * @brief  get the virtual memory peak
	 *
	 * @return the virtual memory peak
	 * 
	 * @note
	 * This value is not stored inside the data buffer
	 */
    Memory_t extractVmPeakSize();

};


typedef std::shared_ptr<argo::monitor::MemoryMonitor> MemoryMonitorPtr;


typedef argo::monitor::Goal<argo::monitor::Memory_t> MemoryGoal;
typedef std::shared_ptr<MemoryGoal> MemoryGoalPtr;





} // namespace monitor

} // namespace argo

#endif /* ARGO_MONITOR_MEMORY_MONITOR_H_ */
