/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARGO_ASRTM_ENUMS_H_
#define ARGO_ASRTM_ENUMS_H_

#include <cstdint>


namespace argo {

/**
 * @brief The direction of the rank
 */
enum class RankObjective:uint8_t {
    Maximize,
    Minimize
};

/**
 * @brief The direction of the rank
 */
enum class CheckType:uint8_t {
    Strictly,
    Loosely
};

/**
 * @brief Indicate in which set an operating point belong
 */
enum class SetType: uint8_t {
  S1 = 0,
  S2,
  NotPresent
};


} // namespace argo

#endif //ARGO_ASRTM_ENUMS_H_
