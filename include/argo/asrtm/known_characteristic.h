/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef ASRTM_KNOWN_CHARACTERISTIC_H
#define ASRTM_KNOWN_CHARACTERISTIC_H

#include <memory>
#include <vector>
#include <list>
#include <functional>
#include <algorithm>


#include "argo/monitor/goal.h"
#include "argo/asrtm/constraint.h"
#include "argo/asrtm/model.h"
#include "argo/asrtm/operating_point.h"



namespace argo { namespace asrtm {
  /**
   * @details 
   * This class rappresents a characteristic obtained
   * at design time, thus it's not associated to an
   * observation window 
   * 
   */
  
  class KnownCharacteristic: public Constraint {
  public:
    
    typedef std::function<bool(OPMetric_t, OPMetric_t)> ComparisonFunctor;
    typedef std::function<bool(KnownCharacteristic*, OperatingPointID_t, OperatingPointID_t)> SortingFunctor;
    
    
    /**
     * @brief Constructor of a known charasteristic with a goal
     * 
     * @param characteristicName The name of the metric or parameter used in this characteristic
     * 
     * @param opList The list of operanting point obtained at design time
     * 
     * @param comparisonFunction The type of comparison used in the goal
     * 
     * @param goalValue The value of the goal
     * 
     * @param isRelaxable If the goal is relaxable
     * 
     */    
    KnownCharacteristic(std::string characteristicName, OperatingPointsList& opList, ComparisonFunction comparisonFunction, OPMetric_t goalValue, bool isRelaxable = false);
    KnownCharacteristic(const KnownCharacteristic& characteristic);
    ~KnownCharacteristic();
    
    
    /**
     * @brief Set the goal as relaxable
     */
    void setRelaxable(std::function<void()> callbackFunction);
    
    
    void changeGoalValue(OPMetric_t newValue);

    OPMetric_t getGoalValue() {
        return goalValue;
    }
    
    /**
     * @brief increment the current goal value
     * 
     * @param increment the relative increment
     */
    void incrementGoal(OPMetric_t increment);
    
    
    
    /**
     * @brief Check if the constraint is satisfied by the operating point
     * 
     * @param opID Is the id of the operatng point
     * 
     */
    bool isAdmissible(OperatingPointID_t opID);
    
    /** 
     * @details 
     * Without an observation windows, this method is used only to 
     * check if the goal is changed
     * 
     * @return If is needed to found another best operating point because
     *         the current OP is no more valid
     */
    bool updateModel(ModelPtr model, OperatingPointID_t currentOP_ID);
    
    
    /**
     * @brief Return an iterator to the best op
     */
    std::list<OperatingPointID_t>::iterator getBestOP();
    
    
    /**
     * @brief Return an iterator to the worst op
     */
    std::list<OperatingPointID_t>::iterator getWorstOP();
    
    /**
     * @brief Return an iterator to the last valid op
     */
    std::list<OperatingPointID_t>::iterator getLimitOP();
    
    /**
     * @brief Compute the distance between the goal and the op
     */
    OPMetric_t getDistance(OperatingPointID_t op);
    
    
    /**
     * @brief Get the value of an Operating Point wrt 
     *        an observable characteristic
     * 
     * @param opID Is the id of the operatng point
     */
    OPMetric_t getOPValue(OperatingPointID_t opID);
    
    
    /**
     * @brief Check the relaxable goal
     */
    void checkRelaxable(OperatingPointID_t currentOP);
    
    
    /**
     * @brief Delete all the observation window's elements
     */
    void clearWindow();

    std::string getName() { return metricName;}
    
    
    
  protected:
    
    /**
     * @brief function used to sort element in the guide vector
     */
    static bool sortingFunction(KnownCharacteristic* obj, OperatingPointID_t lhs, OperatingPointID_t rhs);
    
    /**
     * @details The list of the OPs ordered wrt
     *          this metric
     */
    std::list<OperatingPointID_t> sortedOPs;
    
    /**
     * @details The result achieved by the operating
     *          points wrt this metric. The index of
     *          the vector is the ID of the OP
     */
    std::vector<OPMetric_t> opValues;
    
    /**
     * @details The pointer to op that is the closest op
     *          that satisfy the goal
     */
    std::list<OperatingPointID_t>::iterator limitOP;
    
    /**
     * @brief The relative difference between the actual and the old goal value
     */
    OPMetric_t difference;
    
    
    bool isRelaxable;
    
    /**
     * @brief the callback function called if the goal is not achieved
     */
    std::function<void()> callbackFunction;
    
    
    double goalValue;

    double previousGoalValue;
    
    /**
     * @brief The comparison functor
     */
    ComparisonFunctor comparisonFunction;
    
    /**
     * @details The comparison function of the constraint
     */
    ComparisonFunction cFun;


    std::string metricName;
  };

  typedef std::shared_ptr< KnownCharacteristic > KnownCharacteristicPtr;
  
}  // namespace asrtm

} // namespace argo




#endif // ASRTM_KNOWN_CHARACTERISTIC_H
