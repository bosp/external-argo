/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASRTM_MODEL_H
#define ASRTM_MODEL_H

#include <vector>
#include <list>
#include <memory>
#include "argo/asrtm/asrtm_enums.h"
#include "argo/config.h"


namespace argo { namespace asrtm {
    
  
  /**
   * @details This class describe the model that the asrtm
   * uses to manage the Operating Points. The model uses two 
   * set to rappresent the situation. The sets (S1, S2 and S3) are
   * public for easy access, but if it's needed to add or remove
   * operating points, must be used the class' member function
   * for performance purpose.
   * Note: an op can't belong to both the set at the same time.
   */
  class Model {
  public:
    
    Model(OperatingPointID_t numberOP);
    Model(const Model& model);
    ~Model();
    
    /**
     * @details rappresents the set of operating points that satisfy
     *          all the cosntraints
     */
    std::list<OperatingPointID_t> S1;
    
    /**
     * @details rappresents the set of operating points that should
     *          be added to the set S1 (it's a temporary set, see
     *          ObservableCHaracteristic and OPManager for further details)
     */
    std::list<OperatingPointID_t> S2;
    
    
    /**
     * @brief add an operating point to the set S2
     */
    void addOperatingPoint(OperatingPointID_t opID);
    
    
    /**
     * @brief add an operating point to the set S1
     */
    void addOperatingPointToS1(OperatingPointID_t opID);
    
    /**
     * @brief remove an operating point from S1
     * 
     * @return the iterator of the op next to the one removed
     */
    std::list<OperatingPointID_t>::iterator removeOperatingPointFromS1(OperatingPointID_t opID);

    /**
     * @brief remove an operating point from S2
     * 
     * @return the iterator of the op next to the one removed
     */
    std::list<OperatingPointID_t>::iterator removeOperatingPointFromS2(OperatingPointID_t opID);
    
    /**
     * @brief move the OPs in S2 to S1
     * 
     * @return the iterator of the op in S2 next to the one moved in S1
     */
    std::list<OperatingPointID_t>::iterator commitOP(OperatingPointID_t opID);
    
    
  protected:
    
    /**
     * @brief support vector for fast access
     */
    std::vector<std::list<OperatingPointID_t>::iterator> opFastAccess;
    
    /**
     * @brief support vector for fast access
     */
    std::vector<SetType> opSet;
    
  };
  
  
  typedef std::shared_ptr<Model> ModelPtr;
  
  
} // namespace asrm

} // namespace argo


#if LOG_ENABLED > 0
 #include <string>
 #include <iostream>
 #define PRINT_MODEL(model) {                                                                      \
      INDENT()                                                                                     \
      LOG("S1: ")                                                                                  \
      for (std::list<uint32_t>::iterator it = model->S1.begin(); it !=model->S1.end(); ++it ) {    \
        LOG(std::to_string(*it) + " ")                                                             \
      }                                                                                            \
      NEW_LINE()                                                                                   \
      INDENT()                                                                                     \
      LOG("S2: ")                                                                                  \
      for (std::list<uint32_t>::iterator it = model->S2.begin(); it !=model->S2.end(); ++it ) {    \
        LOG(std::to_string(*it) + " ")                                                             \
      }                                                                                            \
      NEW_LINE()                                                                                   \
    }                                                                                              
#else
 #define PRINT_MODEL(model)
#endif


#endif // ASRTM_MODEL_H
