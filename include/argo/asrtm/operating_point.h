/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARGO_OPERATING_POINT_H_
#define ARGO_OPERATING_POINT_H_

#include <map>
#include <string>
#include <vector>
#include <memory>
#include <list>

#include "argo/config.h"
#include "argo/asrtm/asrtm_enums.h"

/**
 * @brief  The main namespace that wrap the framework
 */
namespace argo { 

/**
 * @brief  The main namespace that wrap the Application-Specific Run-Time Manager
 * 
 */
namespace asrtm {

/**
 * @brief  Operating point class definition
 *
 * @details
 * This class provides a definition of a general operating point.
 * Every operating point is defined by its own application parameters and a set
 * of metrics that describe the performance/behavior obtained using that set.
 *
 * 
 */
class OperatingPoint {
public:
	/**
	 * @brief List of parameters identified by their names and values
	 */
    std::map<std::string, OPParameter_t>  parameters;

	/**
	 * @brief List of metrics identified by their names and values
	 */
    std::map<std::string, OPMetric_t>  metrics;

    /**
	 * @brief List of states that the operating point belongs to
	 */
    std::list<std::string> states;


    /**
     * @brief The identifier of an Operating Point
     */
    argo::asrtm::OperatingPointID_t id;


    OperatingPoint(){}

    OperatingPoint(std::map<std::string, OPParameter_t> parameters,
               std::map<std::string, OPMetric_t>  metrics);



    OperatingPoint(std::map<std::string, OPParameter_t> parameters,
               std::map<std::string, OPMetric_t>  metrics,
               std::list<std::string> states);

    ~OperatingPoint();


    OperatingPoint(const OperatingPoint& op);
	
	
};

/**
 * @brief Typedef for a vector of Operating Point
 */
typedef std::vector<OperatingPoint> OperatingPointsList;

/**
 * @brief Typedef for an instance of parameters configuration value
 */
typedef std::map<std::string, OPParameter_t> ApplicationParameters;

/**
 * @brief Typedef for the performance of an instance of parameters configuration value
 */
typedef std::map<std::string, OPMetric_t> ApplicationMetrics;


} // namespace asrtm


} // namespace argo

#endif /* ARGO_OPERATING_POINT_H_ */
